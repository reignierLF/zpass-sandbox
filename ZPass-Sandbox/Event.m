//
//  Event.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 30/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Event.h"

@implementation Event

-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
        
        _E_companyId = [dictionary valueForKey:@"company_id"];
        _E_createdAt = [dictionary valueForKey:@"created_at"];
        _E_createdById = [dictionary valueForKey:@"created_by_id"];
        _E_credit = [dictionary valueForKey:@"credit"];
        _E_description = [dictionary valueForKey:@"description"];
        _E_draftId = [dictionary valueForKey:@"draft_id"];
        _E_id = [dictionary valueForKey:@"id"];
        _E_imageUrlLarge = [dictionary valueForKey:@"image_large_url"];
        _E_imageUrlMedium = [dictionary valueForKey:@"image_medium_url"];
        _E_imageUrlSmall = [dictionary valueForKey:@"image_small_url"];
        _E_previewDescription = [dictionary valueForKey:@"preview_description"];
        _E_publishedAt = [dictionary valueForKey:@"published_at"];
        _E_status = [dictionary valueForKey:@"status"];
        _E_submittedAt = [dictionary valueForKey:@"submitted_at"];
        _E_title = [dictionary valueForKey:@"title"];
        
        _E_categoryList = [dictionary valueForKey:@"category_list"];
        
        _eventBatches = [[EventBatches alloc] initWithDictionary:dictionary];
        
    }
    return self;
}

@end
