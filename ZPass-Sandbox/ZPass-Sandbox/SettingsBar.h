//
//  SettingsBar.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 5/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SettingsBar : NSObject <UIActionSheetDelegate>

@property (nonatomic, readonly) UIViewController *targetViewController;

-(void)addSettingsBarTo:(UIViewController*)viewController imageName:(NSString*)imageName;

@end
