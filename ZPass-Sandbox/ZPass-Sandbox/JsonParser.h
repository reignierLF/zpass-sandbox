//
//  JsonParser.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 1/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^result)(BOOL);

@interface JsonParser : NSObject

@property (nonatomic, readonly) NSURLRequest *request;
@property (nonatomic, readonly) NSURLResponse *response;
@property (nonatomic, readonly) NSInteger statusCode;
@property (nonatomic, readonly) NSUInteger fileSize;
@property (nonatomic, readonly) NSData *data;
@property (nonatomic, readonly) NSDictionary *jsonDictionary;

-(void)parseJsonURL:(NSString*)urlString parsing:(result)complete;

@end
