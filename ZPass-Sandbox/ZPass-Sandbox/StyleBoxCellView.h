//
//  StyleBoxView.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 28/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StyleBoxDelegate
- (void)styleBoxCellViewButtonPressed:(NSInteger)index image:(UIImage*)image title:(NSString*)title date:(NSString*)date time:(NSString*)time;
@end

@interface StyleBoxCellView : UIView

@property (nonatomic, readonly) UIImage *image;
@property (nonatomic, strong, readwrite) UILabel *title;
@property (nonatomic, strong, readwrite) UILabel *time;
@property (nonatomic, strong, readwrite) UILabel *date;

-(id)initWithFrame:(CGRect)frame image:(NSString*)imageName;

@property (nonatomic, weak) id <StyleBoxDelegate> delegate;

@end
