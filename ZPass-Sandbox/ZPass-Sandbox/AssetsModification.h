//
//  AssetsModification.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 4/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AssetsModification : NSObject

-(UIImage*)imageResizeByBound:(NSString*)imageName targetView:(UIView*)view;

-(UIImage*)imageResizeByCustom:(NSString*)imageName rectSize:(CGSize)size;

@end
