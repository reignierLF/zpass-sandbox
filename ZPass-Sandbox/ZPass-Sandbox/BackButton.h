//
//  BackButton.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 30/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackButton : UIButton

@property (nonatomic, strong) UINavigationController *targetNavigationController;

@end
