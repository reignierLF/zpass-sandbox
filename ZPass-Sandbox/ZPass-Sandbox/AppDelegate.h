//
//  AppDelegate.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 20/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

