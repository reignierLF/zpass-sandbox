//
//  AssetsModification.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 4/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//
/*
 * For custom modification for images or maybe later for other assets
 */

#import "AssetsModification.h"

@implementation AssetsModification

-(UIImage*)imageResizeByBound:(NSString*)imageName targetView:(UIView*)view{
    /*
     Get size of the target view
     */
    UIGraphicsBeginImageContext(view.frame.size);
    /*
     Match the size of the image to target view
     */
    [[UIImage imageNamed:imageName] drawInRect:view.bounds];
    /*
     Apply the new size from target view to image
     */
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    /*
     End of process
     */
    UIGraphicsEndImageContext();
    
    return image;
}

-(UIImage*)imageResizeByCustom:(NSString*)imageName rectSize:(CGSize)size{
    /*
     Set size for image
     */
    UIGraphicsBeginImageContext(size);
    /*
     Set custom frame size for image
     */
    [[UIImage imageNamed:imageName] drawInRect:CGRectMake(0, 0, size.width, size.height)];
    /*
     Apply the new size
     */
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    /*
     End of process
     */
    UIGraphicsEndImageContext();
    
    return image;
}

@end
