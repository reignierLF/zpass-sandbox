//
//  AttendanceCellView.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 5/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttendanceCellView : UIView

@property (nonatomic, assign) UIFont *font;
@property (nonatomic, strong) NSString *rowText;

@property (nonatomic, strong, readwrite) UILabel *rowLabel;
@property (nonatomic, strong, readwrite) UILabel *fullNameLabel;

@end
