//
//  AttendanceViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 23/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "AttendanceViewController.h"

#import "cUIScrollView.h"

@interface AttendanceViewController ()

@end

@implementation AttendanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Dummy value of all participants
    NSMutableArray *participantsArray = [[NSMutableArray alloc] initWithObjects:
                                         @"Name 1",
                                         @"Name 2",
                                         @"Name 3",
                                         @"Name 4",
                                         @"Name 5",
                                         @"Name 6",
                                         @"Name 7",
                                         @"Name 8",
                                         @"Name 9",
                                         @"Name 10",
                                         @"Name 11",
                                         nil];
    float cellHeight = 60;
    
    UILabel *totalScannedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _screenWidth, 50)];
    //totalScannedLabel.backgroundColor = [UIColor greenColor];
    totalScannedLabel.text = [NSString stringWithFormat:@"Total Scanned : %lu", (unsigned long)participantsArray.count];
    totalScannedLabel.font = [UIFont fontWithName:@"Roboto-Light" size:18];
    totalScannedLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:totalScannedLabel];
    
    cUIScrollView *attendanceList = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, totalScannedLabel.frame.origin.y + totalScannedLabel.frame.size.height, _screenWidth, _screenHeight - totalScannedLabel.frame.size.height)];
    attendanceList.contentSize = CGSizeMake(totalScannedLabel.frame.size.width, cellHeight * participantsArray.count);
    //attendanceList.backgroundColor = [UIColor brownColor];
    [self.view addSubview:attendanceList];
    
    AttendanceCellView * acv;
    
    for (int i = 0; i < participantsArray.count; i++) {
        acv = [[AttendanceCellView alloc] initWithFrame:CGRectMake(0, cellHeight * i, attendanceList.frame.size.width, cellHeight)];
        //acv.backgroundColor = [UIColor orangeColor];
        acv.rowLabel.font = [UIFont fontWithName:@"Roboto-Light" size:16];
        acv.rowLabel.text = [NSString stringWithFormat:@"%i",i + 1];
        acv.fullNameLabel.font = acv.rowLabel.font;
        acv.fullNameLabel.text = [NSString stringWithFormat:@"%@",[participantsArray objectAtIndex:i]];
        [attendanceList addSubview:acv];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
