//
//  SettingsBar.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 5/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "SettingsBar.h"

#import "AssetsModification.h"

@implementation SettingsBar

-(id)init{
    self = [super init];
    
    if(self){
        [self settingsBarShowSettingsMenu];
    }
    return self;
}

-(void)addSettingsBarTo:(UIViewController*)viewController imageName:(NSString*)imageName{
    AssetsModification *am = [[AssetsModification alloc] init];
    
    _targetViewController = viewController;
    
    /*
     * Add "Option" button in navigation bar at top right of the screen
     */
    UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc]
                                          initWithImage:[am imageResizeByCustom: imageName rectSize:CGSizeMake(viewController.navigationController.navigationBar.frame.size.height - 4, viewController.navigationController.navigationBar.frame.size.height - 4)]
                                          style:UIBarButtonItemStylePlain
                                          target:self
                                          action:@selector(settingsBarShowSettingsMenu)];
    
    viewController.navigationItem.rightBarButtonItem = settingsBarButton;
}

-(void)settingsBarShowSettingsMenu{
    if (NSClassFromString(@"UIAlertController") != nil) {
        /*
         * For iOS +8 version only
         */
        
        //NSLog(@"UIAlertController can be instantiated");
        
        [self showActionSheet];
    }
    else {
        /*
         * For iOS 7 version only
         */
        
        //NSLog(@"UIAlertController can NOT be instantiated");
        
        UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                  delegate:self
                                                         cancelButtonTitle:@"Cancel"
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles: @"Help / FAQ",
                                       @"Logout",
                                       nil];
        
        [_actionSheet showInView:_targetViewController.view];
    }
}

-(void)showActionSheet{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* wc1 = [UIAlertAction
                          actionWithTitle:@"Help / FAQ"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              //Do some thing here
                              [view dismissViewControllerAnimated:YES completion:nil];
                              
                          }];
    UIAlertAction* wc2 = [UIAlertAction
                          actionWithTitle:@"Logout"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              [view dismissViewControllerAnimated:YES completion:nil];
                              
                              [_targetViewController.navigationController popToRootViewControllerAnimated:YES];
                              
                          }];
    UIAlertAction* wc3 = [UIAlertAction
                          actionWithTitle:@"Cancel"
                          style:UIAlertActionStyleCancel
                          handler:^(UIAlertAction * action)
                          {
                              [view dismissViewControllerAnimated:YES completion:nil];
                              
                          }];
    
    
    [view addAction:wc1];
    [view addAction:wc2];
    [view addAction:wc3];
    [_targetViewController presentViewController:view animated:YES completion:nil];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 2){
        
        [_targetViewController.navigationController popToRootViewControllerAnimated:YES];
    }
}
@end
