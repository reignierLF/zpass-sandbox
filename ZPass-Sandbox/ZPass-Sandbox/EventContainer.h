//
//  EventContainer.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 22/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventContainer : UIView

-(void)drawOval;

@end
