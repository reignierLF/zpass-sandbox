//
//  HomeViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 28/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//
#import "cUIScrollView.h"

#import "HomeViewController.h"
#import "EventViewController.h"
#import "SettingsBar.h"

@interface HomeViewController ()

@property (nonatomic) float screenYOffSet;
@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _screenYOffSet = self.navigationController.navigationBar.frame.size.height + 20;
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - _screenYOffSet;
    
    SettingsBar *sb = [[SettingsBar alloc] init];
    [sb addSettingsBarTo:self imageName:@"settings-ios"];
    
    /*
     * OPTION BUTTON 
     * Add custom "Option" button with navigation bar
     *
     
    UIButton *optionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    optionButton.frame = CGRectMake(screenWidth - 35, 30, 30, 30);
    [optionButton setImage:[UIImage imageNamed:@"settings-ios"] forState:UIControlStateNormal];
    [self.view addSubview:optionButton];
    
    [optionButton addTarget:self action:@selector(optionEvent) forControlEvents:UIControlEventTouchUpInside];
    */
    
    /*
     * Set Background image
     *
     
    UIImageView *eventImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zpass splash 750 copy"]];
    eventImageView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    eventImageView.clipsToBounds = YES;
    
    [self.view insertSubview:eventImageView atIndex:0];
    */
    
    int numberOfTimeEvents = 5;
    
    cUIScrollView *timeListEventsScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, _screenYOffSet, _screenWidth, _screenHeight)];
    timeListEventsScrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    timeListEventsScrollView.contentSize = CGSizeMake(timeListEventsScrollView.frame.size.width, -_screenYOffSet + ((_screenHeight / 3) * numberOfTimeEvents));
    timeListEventsScrollView.delaysContentTouches = NO;
    [self.view addSubview:timeListEventsScrollView];
    
    StyleBoxCellView *timeSlot;
    NSMutableArray *timeSlotArray = [[NSMutableArray alloc] init];

    for (int i = 0; i < numberOfTimeEvents; i++) {
        timeSlot = [[StyleBoxCellView alloc] initWithFrame:CGRectMake(-(_screenWidth / 2), -_screenYOffSet + ((_screenHeight / 3) * i), _screenWidth, (_screenHeight / 3))image:[NSString stringWithFormat:@"natureBG%i",i + 1]];
        timeSlot.delegate = self;
        
        timeSlot.title.text = @"Golden Hour Photography";
        timeSlot.title.font = [UIFont fontWithName:@"Roboto-Light" size:18];
        
        timeSlot.date.text = @"DATE 12 12 12";
        timeSlot.date.font = [UIFont fontWithName:@"Roboto-Thin" size:14];
        
        timeSlot.time.text = @"1:11PM - 11:11PM";
        timeSlot.time.font = [UIFont fontWithName:@"Roboto-Thin" size:14];
        
        timeSlot.alpha = 0;
        timeSlot.tag = i;
        [timeListEventsScrollView addSubview:timeSlot];
        
        [timeSlotArray addObject:timeSlot];
    }
    
    for (int i = 0; i < numberOfTimeEvents; i++) {
        
        [UIView animateWithDuration:1
                              delay:i * 0.25
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             CGRect frame = [[timeSlotArray objectAtIndex:i] frame];
                             
                             [[timeSlotArray objectAtIndex:i] setAlpha:1];
                             [[timeSlotArray objectAtIndex:i] setFrame:CGRectMake(0, frame.origin.y, frame.size.width, frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             if(finished){
                                 
                             }
                         }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 * ==================== Delegates ====================
 */

/*
 * This delegates is from StyleBoxCellView Class
 */

-(void)styleBoxCellViewButtonPressed:(NSInteger)index image:(UIImage *)image title:(NSString *)title date:(NSString *)date time:(NSString *)time{
    EventViewController *eViewController = [[EventViewController alloc] init];
    eViewController.screenYOffSet = _screenYOffSet;
    eViewController.screenWidth = _screenWidth;
    eViewController.screenHeight = _screenHeight;
    eViewController.image = image;
    eViewController.courseTitle = title;
    eViewController.courseDate = date;
    eViewController.courseTime = time;
    [self.navigationController pushViewController:eViewController animated:YES];
    NSLog(@"TAG/Index %li",(long)index);
}

@end
