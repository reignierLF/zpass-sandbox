//
//  AttendanceCellView.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 5/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "AttendanceCellView.h"

@implementation AttendanceCellView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if(self){
        float width = frame.size.width;
        float height = frame.size.height;
        
        _rowLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 50, height)];
        //_rowLabel.backgroundColor = [UIColor greenColor];
        _rowLabel.font = _font;
        [self addSubview:_rowLabel];
        
        _fullNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(_rowLabel.frame.origin.x + _rowLabel.frame.size.width, 0, width - (_rowLabel.frame.origin.x + _rowLabel.frame.size.width + 20), height)];
        //_fullNameLabel.backgroundColor = [UIColor blueColor];
        _fullNameLabel.font = _font;
        [self addSubview:_fullNameLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
