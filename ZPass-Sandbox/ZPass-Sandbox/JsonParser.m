//
//  JsonParser.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 1/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "JsonParser.h"

@implementation JsonParser

-(void)parseJsonURL:(NSString*)urlString parsing:(result)complete{
    /*
     *use this to display "Network indicator"
     */
    //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    _request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];

    [NSURLConnection sendAsynchronousRequest:_request
                     queue:[NSOperationQueue mainQueue]
                     completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                         
                         /*
                          complete(NO) = no json results even there are no server error
                          
                          complete(YES) = with json results use "jsonDictionary" to see the results
                          */
                         
                         _response = response;
                         
                         /*
                          Get status code visit https://www.w3.org/Protocols/HTTP/HTRESP.html
                          for status code details
                          */
                         [self getStatusCode:response];
                         
                         if(_statusCode > 500 || _statusCode > 400){
                             //SERVER ERROR
                             switch(_statusCode){
                                 case 500 :
                                     NSLog(@"Status code : %ld (Internal Error)",(long)_statusCode);
                                     break;
                                 case 501 :
                                     NSLog(@"Status code : %ld (Not implemented)",(long)_statusCode);
                                     break;
                                 case 502 :
                                     NSLog(@"Status code : %ld (Service temporarily overloaded)",(long)_statusCode);
                                     break;
                                 case 503 :
                                     NSLog(@"Status code : %ld (Gateway timeout)",(long)_statusCode);
                                     break;
                                 default :
                                     NSLog(@"Status code : %ld",(long)_statusCode);
                             }
                             
                             complete(NO);
                         }else{
                             _fileSize = data.length;
                             
                             if(_fileSize == 0){
                                 //FILE SIZE ZERO
                             }else{
                                 //JSON RAW DATA
                                 _data = data;
                                 
                                 //JSON PARSED TO DICTIONARY
                                 _jsonDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                   options:0
                                                                                     error:nil];
                                 
                                 if(_jsonDictionary == nil){
                                     NSLog(@"No Json data parsed");
                                     complete(NO);
                                 }else{
                                     complete(YES);
                                 }
                             }
                         }
                         /*
                         if(_statusCode == 500){
                             //SERVER ERROR
                             NSLog(@"Server Error : 500 (Internal Error)");
                             complete(NO);
                         }else if(_statusCode == 501){
                             NSLog(@"Server Error : 501 (Not implemented)");
                             complete(NO);
                         }else if(_statusCode == 502){
                             NSLog(@"Server Error : 502 (Service temporarily overloaded)");
                             complete(NO);
                         }else if(_statusCode == 503){
                             NSLog(@"Server Error : 503 (Gateway timeout)");
                             complete(NO);
                         }else{
                             _fileSize = data.length;
                             
                             if(_fileSize == 0){
                                 //FILE SIZE ZERO
                             }else{
                                 //JSON RAW DATA
                                 _data = data;
                                 
                                 //JSON PARSED TO DICTIONARY
                                 _jsonDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                  options:0
                                                                                    error:nil];
                                 
                                 complete(YES);
                             }
                         }
                        */
    }];
}

-(void)getStatusCode:(NSURLResponse*)response{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    _statusCode = httpResponse.statusCode;
}

@end
