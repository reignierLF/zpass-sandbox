//
//  EventViewController.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 22/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseViewController.h"
#import "ScanViewController.h"
#import "AttendanceViewController.h"

@interface EventViewController : UIViewController <CourseDelegate>

@property (nonatomic, assign) float screenYOffSet;
@property (nonatomic, assign) float screenWidth;
@property (nonatomic, assign) float screenHeight;
@property (nonatomic, assign) UIImage *image;
@property (nonatomic, assign) NSString *courseTitle;
@property (nonatomic, assign) NSString *courseDate;
@property (nonatomic, assign) NSString *courseTime;

@end
