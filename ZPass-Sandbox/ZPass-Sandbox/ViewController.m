//
//  ViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 20/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ViewController.h"
#import "HomeViewController.h"

#import "JsonParser.h"
#import "AssetsModification.h"

@interface ViewController ()

@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    JsonParser *jsp = [[JsonParser alloc] init];
    
    [jsp parseJsonURL:@"http://www.goooooogle.com/" parsing:^(BOOL isComplete){
        if(!isComplete){
            NSLog(@"not complete");
            //Json parsing not complete
        }else{
            NSLog(@"complete");
            //Json parsing complete
            
            NSLog(@"JSON : %@",jsp.jsonDictionary);
        }
    }];
    
    /*
     * This remove the "welcome screen page" from navigation controller and make
     * the "login screen root" of navigation controller
     *
     
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    [viewControllers removeObjectAtIndex:0];
    [self.navigationController setViewControllers:viewControllers];
    */
    
    AssetsModification *as = [[AssetsModification alloc] init];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[as imageResizeByBound:@"zpass login bg" targetView:self.view]];
    
    UIImage *logo = [UIImage imageNamed:@"ZPass Logo"];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logo];
    //logoImageView.backgroundColor = [UIColor greenColor];
    logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:logoImageView];

    /* 2. Constraint to position LeftButton's X*/
    NSLayoutConstraint *logoImageViewXConstraint = [NSLayoutConstraint
                                                 constraintWithItem:logoImageView attribute:NSLayoutAttributeCenterX
                                                 relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.view attribute:
                                                 NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    /* 3. Constraint to position LeftButton's Y*/
    NSLayoutConstraint *logoImageViewYConstraint = [NSLayoutConstraint
                                                 constraintWithItem:logoImageView attribute:NSLayoutAttributeCenterY
                                                 relatedBy:NSLayoutRelationEqual toItem:self.view attribute:
                                                 NSLayoutAttributeCenterY multiplier:1.0f constant:-logoImageView.frame.size.height / 2];
    
    NSLayoutConstraint *logoImageViewsWidth = [NSLayoutConstraint constraintWithItem:logoImageView
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute 
                                                    multiplier:1.0 
                                                      constant:self.view.frame.size.width / 2];
    
    NSLayoutConstraint *logoImageViewHeight = [NSLayoutConstraint constraintWithItem:logoImageView
                                                                 attribute:NSLayoutAttributeHeight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:nil
                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                multiplier:1.0
                                                                  constant:self.view.frame.size.width / 2];
    
    /* 4. Add the constraints to button's superview*/
    [self.view addConstraints:@[ logoImageViewXConstraint,
                                 logoImageViewYConstraint,
                                 logoImageViewsWidth,
                                 logoImageViewHeight]];
    
    _emailTextField = [[UITextField alloc] init];
    _emailTextField.translatesAutoresizingMaskIntoConstraints = NO;
    //emailTextField.backgroundColor = [UIColor greenColor];
    _emailTextField.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    _emailTextField.delegate = self;
    _emailTextField.text = @"Email address";
    _emailTextField.font = [UIFont fontWithName:@"Roboto-Light" size:16];
    [self.view addSubview:_emailTextField];
    
    UIView *emailLineView = [[UIView alloc] init];
    emailLineView.translatesAutoresizingMaskIntoConstraints = NO;
    emailLineView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:emailLineView];
    
    NSLayoutConstraint *lineViewXConstraint = [NSLayoutConstraint
                                               constraintWithItem:emailLineView attribute:NSLayoutAttributeTop
                                               relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:_emailTextField attribute:
                                               NSLayoutAttributeBottom multiplier:1 constant:0];
    
    NSLayoutConstraint *lineViewHeight = [NSLayoutConstraint constraintWithItem:emailLineView
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.0
                                                                       constant:1];
    
    
    NSLayoutConstraint *lineViewConstraintLeft = [NSLayoutConstraint constraintWithItem:emailLineView
                                                                              attribute:NSLayoutAttributeLeft
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:self.view
                                                                              attribute:NSLayoutAttributeLeft
                                                                             multiplier:1.0
                                                                               constant:40];
    
    NSLayoutConstraint *lineViewConstraintRight = [NSLayoutConstraint constraintWithItem:emailLineView
                                                                               attribute:NSLayoutAttributeRight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:self.view
                                                                               attribute:NSLayoutAttributeRight
                                                                              multiplier:1.0
                                                                                constant:-40];
    
    [self.view addConstraints:@[ lineViewXConstraint,
                                 lineViewHeight,
                                 lineViewConstraintLeft,
                                 lineViewConstraintRight]];
    
    _passwordTextField = [[UITextField alloc] init];
    _passwordTextField.translatesAutoresizingMaskIntoConstraints = NO;
    //passwordTextField.backgroundColor = [UIColor greenColor];
    _passwordTextField.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    _passwordTextField.delegate = self;
    _passwordTextField.text = @"Password";
    _passwordTextField.font = [UIFont fontWithName:@"Roboto-Light" size:16];
    [self.view addSubview:_passwordTextField];
    
    UIView *passwordLineView = [[UIView alloc] init];
    passwordLineView.translatesAutoresizingMaskIntoConstraints = NO;
    passwordLineView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:passwordLineView];
    
    NSLayoutConstraint *passwordLineViewConstraintTop = [NSLayoutConstraint
                                               constraintWithItem:passwordLineView attribute:NSLayoutAttributeTop
                                               relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:_passwordTextField attribute:
                                               NSLayoutAttributeBottom multiplier:1 constant:0];
    
    NSLayoutConstraint *passwordLineViewConstraintHeight = [NSLayoutConstraint constraintWithItem:passwordLineView
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.0
                                                                       constant:1];
    
    
    NSLayoutConstraint *passwordLineViewConstraintLeft = [NSLayoutConstraint constraintWithItem:passwordLineView
                                                                              attribute:NSLayoutAttributeLeft
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:self.view
                                                                              attribute:NSLayoutAttributeLeft
                                                                             multiplier:1.0
                                                                               constant:40];
    
    NSLayoutConstraint *passwordLineViewConstraintRight = [NSLayoutConstraint constraintWithItem:passwordLineView
                                                                               attribute:NSLayoutAttributeRight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:self.view
                                                                               attribute:NSLayoutAttributeRight
                                                                              multiplier:1.0
                                                                                constant:-40];
    
    [self.view addConstraints:@[ passwordLineViewConstraintTop,
                                 passwordLineViewConstraintHeight,
                                 passwordLineViewConstraintLeft,
                                 passwordLineViewConstraintRight]];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    [loginButton setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    [loginButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:16]];
    loginButton.translatesAutoresizingMaskIntoConstraints = NO;
    //loginButton.backgroundColor = [UIColor greenColor];
    loginButton.layer.cornerRadius = 5;
    loginButton.layer.borderWidth = 1;
    loginButton.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self.view addSubview:loginButton];
    
    [loginButton addTarget:self action:@selector(loginEvent) forControlEvents:UIControlEventTouchUpInside];
    /*
    NSLayoutConstraint *textFieldTopConstraint = [NSLayoutConstraint
                                                  constraintWithItem:emailTextField attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.view
                                                  attribute:NSLayoutAttributeTop multiplier:1.0 constant:60.0f];
    NSLayoutConstraint *textFieldBottomConstraint = [NSLayoutConstraint
                                                     constraintWithItem:emailTextField attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:logoImageView
                                                     attribute:NSLayoutAttributeTop multiplier:0.8 constant:-10.0f];
    NSLayoutConstraint *textFieldLeftConstraint = [NSLayoutConstraint
                                                   constraintWithItem:emailTextField attribute:NSLayoutAttributeLeft
                                                   relatedBy:NSLayoutRelationEqual toItem:self.view attribute:
                                                   NSLayoutAttributeLeft multiplier:1.0 constant:30.0f];
    NSLayoutConstraint *textFieldRightConstraint = [NSLayoutConstraint 
                                                    constraintWithItem:emailTextField attribute:NSLayoutAttributeRight
                                                    relatedBy:NSLayoutRelationEqual toItem:self.view attribute:
                                                    NSLayoutAttributeRight multiplier:1.0 constant:-30.0f];
    [self.view addConstraints:@[textFieldBottomConstraint ,
                                textFieldLeftConstraint, textFieldRightConstraint, 
                                textFieldTopConstraint]];
    */
    
    // 2. Constraint to position LeftButton's X
    NSLayoutConstraint *emailTextFieldXConstraint = [NSLayoutConstraint
                                                 constraintWithItem:_emailTextField attribute:NSLayoutAttributeTop
                                                 relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:logoImageView attribute:
                                                 NSLayoutAttributeBottom multiplier:1.0 constant:10];
    
    NSLayoutConstraint *emailTextFieldYConstraint = [NSLayoutConstraint constraintWithItem:_emailTextField
                                                                           attribute:NSLayoutAttributeHeight
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:nil
                                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                                          multiplier:1.0
                                                                            constant:30];
    // 3. Constraint to position LeftButton's Y
    //NSLayoutConstraint *emailTextFieldYConstraint = [NSLayoutConstraint
    //                                             constraintWithItem:emailTextField attribute:NSLayoutAttributeBottom
    //                                             relatedBy:NSLayoutRelationEqual toItem:passwordTextField attribute:
    //                                             NSLayoutAttributeTop multiplier:1.0f constant: 10];
    
    NSLayoutConstraint *emailTextFieldWidth = [NSLayoutConstraint constraintWithItem:_emailTextField
                                                                 attribute:NSLayoutAttributeLeft
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self.view
                                                                 attribute:NSLayoutAttributeLeft
                                                                multiplier:1.0
                                                                  constant:50];
    
    NSLayoutConstraint *emailTextFieldHeight = [NSLayoutConstraint constraintWithItem:_emailTextField
                                                                  attribute:NSLayoutAttributeRight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.view
                                                                  attribute:NSLayoutAttributeRight
                                                                 multiplier:1.0
                                                                   constant:-50];

    [self.view addConstraints:@[ emailTextFieldXConstraint,
                                 emailTextFieldYConstraint,
                                 emailTextFieldWidth,
                                 emailTextFieldHeight]];
    
    // 2. Constraint to position LeftButton's X
    NSLayoutConstraint *passwordTextFieldXConstraint = [NSLayoutConstraint
                                                     constraintWithItem:_passwordTextField attribute:NSLayoutAttributeTop
                                                     relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:_emailTextField attribute:
                                                     NSLayoutAttributeBottom multiplier:1.0 constant:20];
    
    NSLayoutConstraint *passwordTextFieldYConstraint = [NSLayoutConstraint constraintWithItem:_passwordTextField
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:nil
                                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                                multiplier:1.0
                                                                                  constant:30];
    // 3. Constraint to position LeftButton's Y
    //NSLayoutConstraint *passwordTextFieldYConstraint = [NSLayoutConstraint
    //                                                 constraintWithItem:passwordTextField attribute:NSLayoutAttributeBottom
    //                                                 relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.view attribute:
    //                                                 NSLayoutAttributeBottom multiplier:1.0f constant: -100];
    
    NSLayoutConstraint *passwordTextFieldWidth = [NSLayoutConstraint constraintWithItem:_passwordTextField
                                                                           attribute:NSLayoutAttributeLeft
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.view
                                                                           attribute:NSLayoutAttributeLeft
                                                                          multiplier:1.0
                                                                            constant:50];
    
    NSLayoutConstraint *passwordTextFieldHeight = [NSLayoutConstraint constraintWithItem:_passwordTextField
                                                                            attribute:NSLayoutAttributeRight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.view
                                                                            attribute:NSLayoutAttributeRight
                                                                           multiplier:1.0
                                                                             constant:-50];
    
    [self.view addConstraints:@[ passwordTextFieldXConstraint,
                                 passwordTextFieldYConstraint,
                                 passwordTextFieldWidth,
                                 passwordTextFieldHeight]];
    
    // 2. Constraint to position LeftButton's X
    NSLayoutConstraint *loginButtonXConstraint = [NSLayoutConstraint
                                                        constraintWithItem:loginButton attribute:NSLayoutAttributeTop
                                                        relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:_passwordTextField attribute:
                                                        NSLayoutAttributeBottom multiplier:1.0 constant:30];
    
    NSLayoutConstraint *loginButtonHeight = [NSLayoutConstraint constraintWithItem:loginButton
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:nil
                                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                                multiplier:1.0
                                                                                  constant:40];
    
    // 3. Constraint to position LeftButton's Y
    NSLayoutConstraint *loginButtonYConstraint = [NSLayoutConstraint
                                                        constraintWithItem:loginButton attribute:NSLayoutAttributeBottom
                                                        relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.view attribute:
                                                        NSLayoutAttributeBottom multiplier:1.0f constant: -80];
    
    NSLayoutConstraint *loginButtonConstraintLeft = [NSLayoutConstraint constraintWithItem:loginButton
                                                                              attribute:NSLayoutAttributeLeft
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:self.view
                                                                              attribute:NSLayoutAttributeLeft
                                                                             multiplier:1.0
                                                                               constant:40];
    
    NSLayoutConstraint *loginButtonConstraintRight = [NSLayoutConstraint constraintWithItem:loginButton
                                                                               attribute:NSLayoutAttributeRight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:self.view
                                                                               attribute:NSLayoutAttributeRight
                                                                              multiplier:1.0
                                                                                constant:-40];
    
    [self.view addConstraints:@[ loginButtonHeight,
                                 loginButtonXConstraint,
                                 loginButtonYConstraint,
                                 loginButtonConstraintLeft,
                                 loginButtonConstraintRight]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

-(void)loginEvent{
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    
    HomeViewController *homeVC = [[HomeViewController alloc] init];
    [self.navigationController pushViewController:homeVC animated:YES];
    
    /*
    if (NSClassFromString(@"UIAlertController") != nil) {
        
        NSLog(@"UIAlertController can be instantiated");
        
        [self actionSheet];
    }
    else {
        
        NSLog(@"UIAlertController can NOT be instantiated");
        
        UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:@"Welcome Screen choices\nHi Team! This function is for developer purpose only, please choose which one do prefer for Welcome Screen or if you dont like any of this two i hope this will help you get an idea, i was hoping we can be dynamic in style and design and not just static when delivering the app for the client... that was just only suggestion :)"
                                                                  delegate:self
                                                         cancelButtonTitle:nil
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles: @"Tradition welcome screen",
                                                                            @"Pop-up welcome screen",
                                                                            @"Parallax welcome screen",
                                                                            nil];
        
        [_actionSheet showInView:self.view];
    }
    */
    NSLog(@"LOGIN");
}

-(void)keyboardWillShow:(NSNotification *)notification {
    float keyBoardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height - 30;
    
    if(self.view.frame.origin.y >= 0){
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - (keyBoardHeight), self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void)keyboardWillBeHidden:(NSNotification *)notification{
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == _passwordTextField){
        _passwordTextField.secureTextEntry = YES;
        NSLog(@"start typing");
    }
    textField.text = @"";
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)emptyTextField:(UITextField *)textField{
    if(textField.text.length == 0){
        if(textField == _emailTextField){
            _emailTextField.text = @"Email address";
        }else if(textField == _passwordTextField){
            _passwordTextField.text = @"Password";
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self emptyTextField:textField];
    
    if(textField == _passwordTextField){
        if([_passwordTextField.text isEqualToString:@"Password"]){
            _passwordTextField.secureTextEntry = NO;
        }else{
            _passwordTextField.secureTextEntry = YES;
        }
    }
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self emptyTextField:textField];
    
    [textField resignFirstResponder];

    return YES;
}

-(void)alertView{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Info"
                                  message:@"You are using UIAlertController"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)actionSheet{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Welcome Screen choices"
                                 message:@"Hi Team! This function is for developer purpose only, please choose which one do prefer for Welcome Screen or if you dont like any of this two i hope this will help you get an idea, i was hoping we can be dynamic in style and design and not just static when delivering the app for the client... that was just only suggestion :)"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* wc1 = [UIAlertAction
                         actionWithTitle:@"Tradition welcome screen"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                             [self welcomeScree:0];
                         }];
    UIAlertAction* wc2 = [UIAlertAction
                             actionWithTitle:@"Pop-up welcome screen"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                                 [self welcomeScree:1];
                             }];
    UIAlertAction* wc3 = [UIAlertAction
                             actionWithTitle:@"Parallax welcome screen"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                                 [self welcomeScree:2];
                             }];
    
    
    [view addAction:wc1];
    [view addAction:wc2];
    [view addAction:wc3];
    [self presentViewController:view animated:YES completion:nil];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self welcomeScree:buttonIndex];
}

-(void)welcomeScree:(NSInteger)index{
    if(index == 0){

    }else if(index == 1){
         //MainViewController *main = [[MainViewController alloc] init];
         //[self.navigationController pushViewController:main animated:YES];
    }else if(index == 2){
    
    }
}
@end
