//
//  ScanViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 23/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ScanViewController.h"
#import "GradientAnimationColor.h"

@interface ScanViewController ()

@property (nonatomic) BOOL isReading;
@property (nonatomic) BOOL pauseInterval;
@property (nonatomic, strong) NSTimer *pauseTimer;

@property (strong, nonatomic) UIView *viewPreview;
@property (strong, nonatomic) UILabel *lblStatus;
//@property (strong, nonatomic) UIButton *bbitemStart; <====== Uncomment out this to activate with button

@property (nonatomic, strong) AVCaptureDevice *device;
@property (strong, nonatomic) AVCaptureDeviceInput* input;
@property (strong, nonatomic) AVCaptureMetadataOutput* output;
@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@property (nonatomic, strong) UIView *successFlashView;
@property (nonatomic, strong) UIView *failedFlashView;

@property (nonatomic) int counter;

//@property (nonatomic, strong) GradientAnimationColor *startScanningContainer; <====== Uncomment out this to activate with button

@property (nonatomic, strong) UIScrollView *dataLogsScrollView;

@property (nonatomic, strong) NSUserDefaults *prefs;

-(BOOL)startReading;
-(void)stopReading;

@end

@implementation ScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.navigationController.navigationBarHidden = NO;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"zpass splash 750 copy"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    _isReading = NO;
    
    _captureSession = nil;
    
    _viewPreview = [[UIView alloc] initWithFrame:CGRectMake(20, 40, self.view.frame.size.width - 40, self.view.frame.size.height - 250)];
    _viewPreview.layer.cornerRadius = 5;
    _viewPreview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_viewPreview];
    
    _lblStatus = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, _viewPreview.frame.size.width, 20)];
    //_lblStatus.backgroundColor = [UIColor blackColor];
    _lblStatus.textColor = [UIColor greenColor];
    _lblStatus.font = [UIFont systemFontOfSize:12];
    //_lblStatus.text = @"SCAN OFF";
    //[_viewPreview.layer addSublayer:_lblStatus.layer];
    
    /* <====== Uncomment out this to activate with button
    _startScanningContainer = [[GradientAnimationColor alloc] initWithFrame:CGRectMake(0, 0, _viewPreview.frame.size.width, _viewPreview.frame.size.height)];
    _startScanningContainer.layer.cornerRadius = _viewPreview.layer.cornerRadius;
    //_startScanningContainer.backgroundColor = [UIColor whiteColor];
    [_viewPreview.layer addSublayer:_startScanningContainer.layer];
    
    UIImage *logo = [UIImage imageNamed:@"ZPass Logo"];
    
    UIImageView *logoZpassImageView = [[UIImageView alloc] initWithImage:logo];
    logoZpassImageView.frame = CGRectMake(_startScanningContainer.frame.size.width - ((_startScanningContainer.frame.size.width / 2) + 50), _startScanningContainer.frame.size.height - ((_startScanningContainer.frame.size.height / 2) + 50), 100, 100);
    //logoZpassImageView.backgroundColor = [UIColor whiteColor];
    [_startScanningContainer addSubview:logoZpassImageView];
    
    UILabel *tapLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _viewPreview.frame.size.height - (_viewPreview.frame.size.height / 4), _viewPreview.frame.size.width, _viewPreview.frame.size.height / 4)];
    //tapLabel.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.5];
    tapLabel.textAlignment = NSTextAlignmentCenter;
    tapLabel.font = [UIFont systemFontOfSize:16];
    tapLabel.text = @"Tap to begin or stop scanning";
    [_startScanningContainer addSubview:tapLabel];
    
    
    _bbitemStart = [UIButton buttonWithType:UIButtonTypeSystem];
    _bbitemStart.frame = _viewPreview.frame;
    _bbitemStart.layer.cornerRadius = _viewPreview.layer.cornerRadius;
    //_bbitemStart.backgroundColor = [[UIColor purpleColor] colorWithAlphaComponent:0.2];
    [_bbitemStart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[_bbitemStart setTitle:@"START" forState:UIControlStateNormal];
    [self.view addSubview:_bbitemStart];
    
    [_bbitemStart addTarget:self action:@selector(startStopReading:) forControlEvents:UIControlEventTouchUpInside];
    */ //<====== Uncomment out this to activate with button
    
    _dataLogsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(_viewPreview.frame.origin.x, _viewPreview.frame.origin.y + _viewPreview.frame.size.height + 10, _viewPreview.frame.size.width, (self.view.frame.size.height - 60) - (_viewPreview.frame.origin.y + _viewPreview.frame.size.height + 10))];
    _dataLogsScrollView.delegate = self;
    _dataLogsScrollView.layer.cornerRadius = _viewPreview.layer.cornerRadius;
    _dataLogsScrollView.contentSize = CGSizeMake(_dataLogsScrollView.frame.size.width, _dataLogsScrollView.frame.size.height * 2);
    _dataLogsScrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [self.view addSubview:_dataLogsScrollView];
    
    _prefs = [NSUserDefaults standardUserDefaults];
    
    _dataLogsMutableArray = [_prefs mutableArrayValueForKey:@"datalogs"];
    
    if(_dataLogsMutableArray.count == 0){
        _dataLogsMutableArray = [[NSMutableArray alloc] init];
    }else{
        for (int i = 0; i < _dataLogsMutableArray.count; i++) {
            [self scanningLogs:[NSString stringWithFormat:@"%@",[_dataLogsMutableArray objectAtIndex:i]] row:i];
        }
    }
    
    NSLog(@"on load logs -> %@", _dataLogsMutableArray);
    
    /*
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = startScanningContainer.bounds;
    gradient.cornerRadius = startScanningContainer.layer.cornerRadius;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor lightTextColor] CGColor], (id)[[UIColor lightGrayColor] CGColor], nil];
    gradient.startPoint = CGPointMake(1, 1);
    gradient.endPoint = CGPointMake(0, 0);
    [startScanningContainer.layer insertSublayer:gradient atIndex:0];
    
    [UIView animateWithDuration:3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         gradient.startPoint = CGPointMake(0, 0);
                         gradient.endPoint = CGPointMake(1, 1);
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                         }
                     }];
    */
    [self startStopReading];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewWillDisappear:(BOOL)animated{
    //self.navigationController.navigationBarHidden = YES;
    
    [_prefs setObject:_dataLogsMutableArray forKey:@"datalogs"];
    [_prefs synchronize];
    
    NSLog(@"data logs saved");
}

//- (void)startStopReading:(id)sender { <====== Uncomment out this to activate with button
- (void)startStopReading{
    NSLog(@"start/stop");
    
    if (!_isReading) {
        if ([self startReading]) {
            //[_bbitemStart setTitle:@"Stop" forState:UIControlStateNormal];
            [_lblStatus setText:@"Scanning QR code..."];
            //[_startScanningContainer.layer removeFromSuperlayer]; <====== Uncomment out this to activate with button
            
            NSLog(@"start");
        }
    }
    else{
        [self stopReading];
        //[_bbitemStart setTitle:@"Start!" forState:UIControlStateNormal];
        [_lblStatus setText:@"SCAN OFF"];
        //[_viewPreview.layer addSublayer:_startScanningContainer.layer]; <====== Uncomment out this to activate with button
        
        NSLog(@"stop");
    }
    
    _isReading = !_isReading;
}

- (BOOL)startReading {
    NSError *error;
    
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    _videoPreviewLayer.cornerRadius = _viewPreview.layer.cornerRadius;
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    [_videoPreviewLayer addSublayer:_lblStatus.layer];
    
    _successFlashView = [[UIView alloc] initWithFrame:_videoPreviewLayer.frame];
    _successFlashView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    _successFlashView.layer.cornerRadius = _videoPreviewLayer.cornerRadius;
    _successFlashView.userInteractionEnabled = NO;
    _successFlashView.hidden = YES;
    [_viewPreview.layer addSublayer:_successFlashView.layer];
    
    _failedFlashView = [[UIView alloc] initWithFrame:_successFlashView.frame];
    _failedFlashView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.5];
    _failedFlashView.layer.cornerRadius = _successFlashView.layer.cornerRadius;
    _failedFlashView.userInteractionEnabled = NO;
    _failedFlashView.hidden = YES;
    [_viewPreview.layer addSublayer:_failedFlashView.layer];
    
    [_captureSession startRunning];
    
    return YES;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{

    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode] && !_pauseInterval) {
            [_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:@"Processing QR code..." waitUntilDone:NO];
            //[_lblStatus performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSMutableArray *qrCodeDataArray = [[NSMutableArray alloc] init];
            [qrCodeDataArray addObject:[dateFormatter stringFromDate:[NSDate date]]];
            [qrCodeDataArray addObject:[metadataObj stringValue]];
            
            if([[metadataObj stringValue] isEqualToString:@"Livefitter"]){
                [qrCodeDataArray addObject:@"verify"];
                [self performSelectorOnMainThread:@selector(successVerify:) withObject:qrCodeDataArray waitUntilDone:NO];
            }else{
                [qrCodeDataArray addObject:@"decline"];
                [self performSelectorOnMainThread:@selector(failedVerify:) withObject:qrCodeDataArray waitUntilDone:NO];
            }
            
            [_dataLogsMutableArray addObject:[[qrCodeDataArray valueForKey:@"description"] componentsJoinedByString:@"|"] ];
            
            //NSLog(@"allObjects %@",_dataLogsMutableArray);
            
            //[_dataLogsMutableArray addObject:verifyStatus];
            
            //[self performSelectorOnMainThread:@selector(scanningLogs:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            
            NSLog(@"%@", [metadataObj stringValue]);
            
            [self performSelectorOnMainThread:@selector(pauseReading) withObject:nil waitUntilDone:NO];
            //[self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            //[_bbitemStart performSelectorOnMainThread:@selector(setTitle:forState:) withObject:@"Start!" waitUntilDone:NO];
            
            //_isReading = NO;
            NSLog(@"counter : %i",_counter++);
        }
    }
}

-(void)stopReading{
    [_captureSession stopRunning];
    _captureSession = nil;
    
    [_videoPreviewLayer removeFromSuperlayer];
    
    [_pauseTimer invalidate];
    _pauseTimer = nil;
}

-(void)pauseReading{
    _pauseInterval = YES;
    //_bbitemStart.userInteractionEnabled = NO;
    
    _pauseTimer = [NSTimer scheduledTimerWithTimeInterval:3.0
                                                   target:self selector:@selector(resumeReading) userInfo:nil repeats:YES];
}

-(void)resumeReading{
    _lblStatus.text = @"Scanning QR code...";
    _pauseInterval = NO;
    [_pauseTimer invalidate];
    _pauseTimer = nil;
    //_bbitemStart.userInteractionEnabled = YES;
}

-(void)successVerify:(NSMutableArray*)qrCodeData{
    _successFlashView.hidden = NO;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _successFlashView.backgroundColor = [_successFlashView.backgroundColor colorWithAlphaComponent:0.0];
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             _successFlashView.hidden = YES;
                             _successFlashView.backgroundColor = [_successFlashView.backgroundColor colorWithAlphaComponent:0.5];
                             
                             [self scanningLogs:[[qrCodeData valueForKey:@"description"] componentsJoinedByString:@"|"] row:(int)_dataLogsMutableArray.count - 1];
                         }
                     }];
}

-(void)failedVerify:(NSMutableArray*)qrCodeData{
    _failedFlashView.hidden = NO;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _failedFlashView.backgroundColor = [_failedFlashView.backgroundColor colorWithAlphaComponent:0.0];
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             _failedFlashView.hidden = YES;
                             _failedFlashView.backgroundColor = [_failedFlashView.backgroundColor colorWithAlphaComponent:0.5];
                             
                             [self scanningLogs:[[qrCodeData valueForKey:@"description"] componentsJoinedByString:@"|"] row:(int)_dataLogsMutableArray.count - 1];
                         }
                     }];
}

-(void)scanningLogs:(NSString*)param row:(int)row{
    
    NSLog(@"PARAM = %@", param);
    
    _dataLogsScrollView.contentSize = CGSizeMake(_dataLogsScrollView.frame.size.width, 15 * (row + 1));
    
    if(_dataLogsScrollView.contentSize.height > _dataLogsScrollView.frame.size.height){
        CGPoint scrollDownPoint = CGPointMake(0, _dataLogsScrollView.contentSize.height - _dataLogsScrollView.bounds.size.height);
        [_dataLogsScrollView setContentOffset:scrollDownPoint animated:YES];
    }
    
    /*
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    */
    UILabel *logLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, row * 15, _dataLogsScrollView.frame.size.width, 15)];
    logLabel.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.3];
    logLabel.textColor = [UIColor whiteColor];
    logLabel.font = [UIFont systemFontOfSize:10];
    //logLabel.text = [NSString stringWithFormat:@"%@ | %@",[dateFormatter stringFromDate:[NSDate date]],param];
    logLabel.text = param;

    /*
    if([stats isEqualToString:@"verify"]){
        logLabel.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
    }else{
        logLabel.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.5];
    }
     */
    [_dataLogsScrollView addSubview:logLabel];
}

@end
