//
//  WelcomeScreenViewController.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 27/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeScreenViewController : UIViewController <UIScrollViewDelegate>

@end
