//
//  EventContainer.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 22/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "EventContainer.h"

@implementation EventContainer


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self drawOval];
}

-(void)drawOval{
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    UIView *ovalView = [[UIView alloc] initWithFrame:CGRectMake(0 - (width / 8), 0 - (width / 4), width + (width / 4), width / 2)];
    ovalView.layer.cornerRadius = ovalView.frame.size.width / 2;
    ovalView.backgroundColor = self.backgroundColor;
    //ovalView.backgroundColor = [UIColor orangeColor];
    [self addSubview:ovalView];
}

@end
