//
//  CourseViewController.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 23/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CourseDelegate
- (void)courseStartScan;
@end

@interface CourseViewController : UIViewController

@property (nonatomic, assign) float screenYOffSet;
@property (nonatomic, assign) float screenWidth;
@property (nonatomic, assign) float screenHeight;
@property (nonatomic, assign) UIImage *image;
@property (nonatomic, assign) NSString *courseTitle;
@property (nonatomic, assign) NSString *courseDate;
@property (nonatomic, assign) NSString *courseTime;

@property (nonatomic, weak) id <CourseDelegate> delegate;

@end
