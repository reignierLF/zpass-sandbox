//
//  StyleBoxView.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 28/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//
/*
 * Custom Cell for scrollview and not for TableViewController
 * just add custom frame or base on scrollview content size
 */

#import "StyleBoxCellView.h"

@implementation StyleBoxCellView
/*
 * This is a stylish Cell
 *
 
-(id)initWithFrame:(CGRect)frame image:(NSString*)imageName{
    self = [super initWithFrame:frame];
    
    if(self){
        float styleBoxWidth = frame.size.width;
        float styleBoxHeight = frame.size.height;
        
        UIImageView *eventImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        //UIImageView *eventImageView = [[UIImageView alloc] init];
        eventImageView.frame = CGRectMake(0, 10, frame.size.width, frame.size.height - 10);
        eventImageView.contentMode = UIViewContentModeScaleAspectFill;
        eventImageView.alpha = 0.8;
        eventImageView.clipsToBounds = YES;
        
        _image = eventImageView.image;
        
        //eventImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:eventImageView];
        
        CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color1 = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        
        CGFloat hue2 = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation2 = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness2 = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color2 = [UIColor colorWithHue:hue2 saturation:saturation2 brightness:brightness2 alpha:1];

        UIView *styleBox1 = [[UIView alloc] init];
        styleBox1.backgroundColor = color1;
        styleBox1.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:styleBox1];
        
        NSLayoutConstraint *styleBox1PaddingLeft = [NSLayoutConstraint
                                                        constraintWithItem:styleBox1 attribute:NSLayoutAttributeLeft
                                                        relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                        NSLayoutAttributeLeft multiplier:1.0 constant:20];
        
        NSLayoutConstraint *styleBox1PaddingRight = [NSLayoutConstraint
                                                    constraintWithItem:styleBox1 attribute:NSLayoutAttributeRight
                                                    relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                    NSLayoutAttributeRight multiplier:1.0 constant:-(styleBoxWidth / 4)];
        
        NSLayoutConstraint *styeBox1Top = [NSLayoutConstraint constraintWithItem:styleBox1
                                                                          attribute:NSLayoutAttributeTop
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self
                                                                          attribute:NSLayoutAttributeTop
                                                                         multiplier:1.0
                                                                           constant:20];
        
        NSLayoutConstraint *styeBox1Bottom = [NSLayoutConstraint constraintWithItem:styleBox1
                                                                       attribute:NSLayoutAttributeBottom
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeBottom
                                                                      multiplier:1.0
                                                                        constant:-(styleBoxHeight / 3)];
        

        [self addConstraints:@[ styleBox1PaddingLeft,
                                    styleBox1PaddingRight,
                                    styeBox1Top,
                                    styeBox1Bottom]];
        
        UIView *styleBox2 = [[UIView alloc] init];
        styleBox2.translatesAutoresizingMaskIntoConstraints = NO;
        styleBox2.backgroundColor = color2;
        [self addSubview:styleBox2];
        
        NSLayoutConstraint *styleBox2PaddingLeft = [NSLayoutConstraint
                                                    constraintWithItem:styleBox2 attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                    NSLayoutAttributeLeft multiplier:1.0 constant:(styleBoxWidth / 4)];
        
        NSLayoutConstraint *styleBox2PaddingRight = [NSLayoutConstraint
                                                     constraintWithItem:styleBox2 attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                     NSLayoutAttributeRight multiplier:1.0 constant:-20];
        
        NSLayoutConstraint *styeBox2Top = [NSLayoutConstraint constraintWithItem:styleBox2
                                                                       attribute:NSLayoutAttributeTop
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeTop
                                                                      multiplier:1.0
                                                                        constant:30];
        
        NSLayoutConstraint *styeBox2Bottom = [NSLayoutConstraint constraintWithItem:styleBox2
                                                                          attribute:NSLayoutAttributeBottom
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self
                                                                          attribute:NSLayoutAttributeBottom
                                                                         multiplier:1.0
                                                                           constant:-((styleBoxHeight / 3) - 10)];
        
        
        [self addConstraints:@[ styleBox2PaddingLeft,
                                styleBox2PaddingRight,
                                styeBox2Top,
                                styeBox2Bottom]];
        
        //UIView *detailView = [[UIView alloc] initWithFrame:CGRectMake(styleBox1.frame.origin.x + 20, styleBox2.frame.origin.y + 20, styleBoxWidth - (styleBoxWidth / 4), styleBoxHeight - (styleBoxHeight / 2))];
        UIView *detailView = [[UIView alloc] init];
        detailView.translatesAutoresizingMaskIntoConstraints = NO;
        detailView.backgroundColor = [UIColor whiteColor];
        [self addSubview:detailView];
        
        NSLayoutConstraint *detailViewPaddingLeft = [NSLayoutConstraint
                                                    constraintWithItem:detailView attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                    NSLayoutAttributeLeft multiplier:1.0 constant:40];
        
        NSLayoutConstraint *detailViewPaddingRight = [NSLayoutConstraint
                                                     constraintWithItem:detailView attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                     NSLayoutAttributeRight multiplier:1.0 constant:-40];
        
        NSLayoutConstraint *detailViewTop = [NSLayoutConstraint constraintWithItem:detailView
                                                                       attribute:NSLayoutAttributeTop
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeTop
                                                                      multiplier:1.0
                                                                        constant:40];
        
        NSLayoutConstraint *detailViewBottom = [NSLayoutConstraint constraintWithItem:detailView
                                                                          attribute:NSLayoutAttributeBottom
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self
                                                                          attribute:NSLayoutAttributeBottom
                                                                         multiplier:1.0
                                                                           constant:-20];
        
        
        [self addConstraints:@[ detailViewPaddingLeft,
                                detailViewPaddingRight,
                                detailViewTop,
                                detailViewBottom]];
        
        UILabel *label = [[UILabel alloc] init];
        label.backgroundColor = [UIColor lightTextColor];
        label.textColor = [UIColor grayColor];
        label.font = [UIFont systemFontOfSize:20];
        //label.textAlignment = NSTextAlignmentCenter;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.text = @"   4:00 PM - 12:00 PM";
        [self addSubview:label];
        
        NSLayoutConstraint *labeladdingLeft = [NSLayoutConstraint
                                                     constraintWithItem:label attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                     NSLayoutAttributeLeft multiplier:1.0 constant:40];
        
        NSLayoutConstraint *labelPaddingRight = [NSLayoutConstraint
                                                      constraintWithItem:label attribute:NSLayoutAttributeRight
                                                      relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                      NSLayoutAttributeRight multiplier:1.0 constant:-40];
        
        NSLayoutConstraint *labelTop = [NSLayoutConstraint constraintWithItem:label
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1.0
                                                                          constant:40];
        
        NSLayoutConstraint *labelBottom = [NSLayoutConstraint constraintWithItem:label
                                                                            attribute:NSLayoutAttributeBottom
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self
                                                                            attribute:NSLayoutAttributeBottom
                                                                           multiplier:1.0
                                                                             constant:-20];
        
        [self addConstraints:@[ labeladdingLeft,
                                labelPaddingRight,
                                labelTop,
                                labelBottom]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(0, 0, styleBoxWidth, styleBoxHeight - 10);
        //button.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
        [self addSubview:button];
        
        [button addTarget:self action:@selector(StyleBoxCellViewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}
*/
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/*
 * Plain image background and a rounded box for detail
 */

-(id)initWithFrame:(CGRect)frame image:(NSString*)imageName{
    self = [super initWithFrame:frame];
    
    if(self){
        float styleBoxWidth = frame.size.width;
        float styleBoxHeight = frame.size.height;
        
        UIImageView *eventImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        eventImageView.frame = CGRectMake(0, 0, styleBoxWidth, styleBoxHeight - 2);
        eventImageView.contentMode = UIViewContentModeScaleAspectFill;
        eventImageView.alpha = 0.8;
        eventImageView.clipsToBounds = YES;
        
        _image = eventImageView.image;
        
        [self addSubview:eventImageView];
        
        UIView *detailView = [[UIView alloc] init];
        detailView.translatesAutoresizingMaskIntoConstraints = NO;
        detailView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
        detailView.layer.cornerRadius = 10;
        [self addSubview:detailView];
        
        NSLayoutConstraint *detailViewPaddingLeft = [NSLayoutConstraint
                                                     constraintWithItem:detailView attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                     NSLayoutAttributeLeft multiplier:1.0 constant:20];
        
        NSLayoutConstraint *detailViewPaddingRight = [NSLayoutConstraint
                                                      constraintWithItem:detailView attribute:NSLayoutAttributeRight
                                                      relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                      NSLayoutAttributeRight multiplier:1.0 constant:-20];
        
        NSLayoutConstraint *detailViewTop = [NSLayoutConstraint constraintWithItem:detailView
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1.0
                                                                          constant:20];
        
        NSLayoutConstraint *detailViewBottom = [NSLayoutConstraint constraintWithItem:detailView
                                                                            attribute:NSLayoutAttributeBottom
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self
                                                                            attribute:NSLayoutAttributeBottom
                                                                           multiplier:1.0
                                                                             constant:-20];
        
        
        [self addConstraints:@[ detailViewPaddingLeft,
                                detailViewPaddingRight,
                                detailViewTop,
                                detailViewBottom]];
        
        float textSideMargin = 40;
        
        _date = [[UILabel alloc] init];
        //_date.backgroundColor = [UIColor purpleColor];
        _date.textColor = [UIColor blackColor];
        _date.textAlignment = NSTextAlignmentCenter;
        _date.translatesAutoresizingMaskIntoConstraints = NO;
        _date.text = @"Date";
        [self addSubview:_date];
        
        NSLayoutConstraint *datePaddingLeft = [NSLayoutConstraint
                                               constraintWithItem:_date attribute:NSLayoutAttributeLeft
                                               relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                               NSLayoutAttributeLeft multiplier:1.0 constant:textSideMargin];
        
        NSLayoutConstraint *datePaddingRight = [NSLayoutConstraint
                                                constraintWithItem:_date attribute:NSLayoutAttributeRight
                                                relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                NSLayoutAttributeRight multiplier:1.0 constant:-textSideMargin];
        
        NSLayoutConstraint *dateHeightConstraint = [NSLayoutConstraint constraintWithItem:_date
                                                                                attribute:NSLayoutAttributeHeight
                                                                                relatedBy:NSLayoutRelationEqual
                                                                                   toItem:nil
                                                                                attribute:NSLayoutAttributeNotAnAttribute
                                                                               multiplier:1.0
                                                                                 constant:20];
        
        NSLayoutConstraint *dateYConstraint = [NSLayoutConstraint
                                               constraintWithItem:_date attribute:NSLayoutAttributeCenterY
                                               relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                               NSLayoutAttributeCenterY multiplier:1.0f constant:10];
        
        /*
         NSLayoutConstraint *labelTop = [NSLayoutConstraint constraintWithItem:title
         attribute:NSLayoutAttributeCenterY
         relatedBy:NSLayoutRelationEqual
         toItem:self
         attribute:NSLayoutAttributeTop
         multiplier:1.0
         constant:40];
         
         NSLayoutConstraint *labelBottom = [NSLayoutConstraint constraintWithItem:title
         attribute:NSLayoutAttributeBottom
         relatedBy:NSLayoutRelationEqual
         toItem:self
         attribute:NSLayoutAttributeBottom
         multiplier:1.0
         constant:-20];
         */
        [self addConstraints:@[ datePaddingLeft,
                                datePaddingRight,
                                dateYConstraint,
                                dateHeightConstraint]];
        
        _title = [[UILabel alloc] init];
        //_title.backgroundColor = [UIColor greenColor];
        _title.textColor = [UIColor blackColor];
        _title.textAlignment = NSTextAlignmentCenter;
        _title.translatesAutoresizingMaskIntoConstraints = NO;
        _title.text = @"Title";
        _title.numberOfLines = 0;
        [self addSubview:_title];
        
        NSLayoutConstraint *titlePaddingLeft = [NSLayoutConstraint
                                                constraintWithItem:_title attribute:NSLayoutAttributeLeft
                                                relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                NSLayoutAttributeLeft multiplier:1.0 constant:textSideMargin];
        
        NSLayoutConstraint *titlePaddingRight = [NSLayoutConstraint
                                                 constraintWithItem:_title attribute:NSLayoutAttributeRight
                                                 relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                 NSLayoutAttributeRight multiplier:1.0 constant:-textSideMargin];
        
        NSLayoutConstraint *titleHeightConstraint = [NSLayoutConstraint constraintWithItem:_title
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                 relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                                    toItem:nil
                                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                                multiplier:1.0
                                                                                  constant:40];
        
        NSLayoutConstraint *titleYConstraint = [NSLayoutConstraint
                                                constraintWithItem:_title attribute:NSLayoutAttributeBottom
                                                relatedBy:NSLayoutRelationEqual toItem:_date attribute:
                                                NSLayoutAttributeTop multiplier:1.0f constant:0];
        
        /*
         NSLayoutConstraint *labelTop = [NSLayoutConstraint constraintWithItem:title
         attribute:NSLayoutAttributeCenterY
         relatedBy:NSLayoutRelationEqual
         toItem:self
         attribute:NSLayoutAttributeTop
         multiplier:1.0
         constant:40];
         
         NSLayoutConstraint *labelBottom = [NSLayoutConstraint constraintWithItem:title
         attribute:NSLayoutAttributeBottom
         relatedBy:NSLayoutRelationEqual
         toItem:self
         attribute:NSLayoutAttributeBottom
         multiplier:1.0
         constant:-20];
         */
        [self addConstraints:@[ titlePaddingLeft,
                                titlePaddingRight,
                                titleYConstraint,
                                titleHeightConstraint]];
        
        _time = [[UILabel alloc] init];
        //_time.backgroundColor = [UIColor yellowColor];
        _time.textColor = [UIColor blackColor];
        _time.textAlignment = NSTextAlignmentCenter;
        _time.translatesAutoresizingMaskIntoConstraints = NO;
        _time.text = @"Time";
        [self addSubview:_time];
        
        NSLayoutConstraint *timePaddingLeft = [NSLayoutConstraint
                                                constraintWithItem:_time attribute:NSLayoutAttributeLeft
                                                relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                NSLayoutAttributeLeft multiplier:1.0 constant:textSideMargin];
        
        NSLayoutConstraint *timePaddingRight = [NSLayoutConstraint
                                                 constraintWithItem:_time attribute:NSLayoutAttributeRight
                                                 relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                 NSLayoutAttributeRight multiplier:1.0 constant:-textSideMargin];
        
        NSLayoutConstraint *timeHeightConstraint = [NSLayoutConstraint constraintWithItem:_time
                                                                                 attribute:NSLayoutAttributeHeight
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:nil
                                                                                 attribute:NSLayoutAttributeNotAnAttribute
                                                                                multiplier:1.0
                                                                                  constant:20];
        
        NSLayoutConstraint *timeYConstraint = [NSLayoutConstraint
                                               constraintWithItem:_time attribute:NSLayoutAttributeTop
                                               relatedBy:NSLayoutRelationEqual toItem:_date attribute:
                                               NSLayoutAttributeBottom multiplier:1.0f constant:0];
        
        /*
         NSLayoutConstraint *labelTop = [NSLayoutConstraint constraintWithItem:title
         attribute:NSLayoutAttributeCenterY
         relatedBy:NSLayoutRelationEqual
         toItem:self
         attribute:NSLayoutAttributeTop
         multiplier:1.0
         constant:40];
         
         NSLayoutConstraint *labelBottom = [NSLayoutConstraint constraintWithItem:title
         attribute:NSLayoutAttributeBottom
         relatedBy:NSLayoutRelationEqual
         toItem:self
         attribute:NSLayoutAttributeBottom
         multiplier:1.0
         constant:-20];
         */
        [self addConstraints:@[ timePaddingLeft,
                                timePaddingRight,
                                timeYConstraint,
                                timeHeightConstraint]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(0, 0, styleBoxWidth, eventImageView.frame.size.height);
        //button.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
        [self addSubview:button];
        
        [button addTarget:self action:@selector(styleBoxCellViewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

-(void)styleBoxCellViewButtonPressed:(UIButton*)sender{
    [self.delegate styleBoxCellViewButtonPressed:self.tag image:_image title:_title.text date:_date.text time:_time.text];
}
@end
