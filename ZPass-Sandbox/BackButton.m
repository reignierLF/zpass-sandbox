//
//  BackButton.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 30/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "BackButton.h"

@implementation BackButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [self setImage:[UIImage imageNamed:@"backButton"] forState:UIControlStateNormal];
    self.adjustsImageWhenHighlighted = NO;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - (self.frame.size.width / 4), self.frame.size.height - (self.frame.size.height / 10))];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = self.titleLabel.font;
    label.text = self.titleLabel.text;
    //label.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.5];
    [self addSubview:label];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_targetNavigationController popViewControllerAnimated:YES];
}

@end
