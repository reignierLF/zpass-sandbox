//
//  CourseViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 23/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "CourseViewController.h"
#import "EventContainer.h" // Currently disable

@interface CourseViewController ()

@end

@implementation CourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];

    /*
     * Instantiate image for event
     */
    
    UIImageView *eventImageView = [[UIImageView alloc] initWithImage:_image];
    eventImageView.frame = CGRectMake(0, 0, _screenWidth, _screenHeight / 3);
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    eventImageView.clipsToBounds = YES;
    //eventImageView.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:eventImageView];

    /*
     * Old layout design for CourseAttendance
     *
     
    EventContainer *ec = [[EventContainer alloc] init];
    ec.translatesAutoresizingMaskIntoConstraints = NO;
    ec.backgroundColor = [UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1];
    [self.view addSubview:ec];
    
    NSLayoutConstraint *ecHeightConstraint = [NSLayoutConstraint constraintWithItem:ec
                                                                          attribute:NSLayoutAttributeHeight
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:nil
                                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                                         multiplier:1.0
                                                                           constant:self.view.frame.size.height];
    
    NSLayoutConstraint *ecWidthConstraint = [NSLayoutConstraint constraintWithItem:ec
                                                                         attribute:NSLayoutAttributeWidth
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:nil
                                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                                        multiplier:1.0
                                                                          constant:self.view.frame.size.width];
    
    NSLayoutConstraint *ecTopConstraint = [NSLayoutConstraint
                                                  constraintWithItem:ec attribute:NSLayoutAttributeTop
                                                  relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.view attribute:
                                                  NSLayoutAttributeTop multiplier:1.0 constant:self.view.frame.size.height / 2.25];
    
    NSLayoutConstraint *ecBottomConstraint = [NSLayoutConstraint
                                                  constraintWithItem:ec attribute:NSLayoutAttributeBottom
                                                  relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.view attribute:
                                                  NSLayoutAttributeBottom multiplier:1.0f constant: 0];
    
    NSLayoutConstraint *ecLeftConstraint = [NSLayoutConstraint constraintWithItem:ec
                                                                                 attribute:NSLayoutAttributeLeft
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:self.view
                                                                                 attribute:NSLayoutAttributeLeft
                                                                                multiplier:1.0
                                                                                  constant:0];
    
    NSLayoutConstraint *ecRightConstraint = [NSLayoutConstraint constraintWithItem:ec
                                                                                  attribute:NSLayoutAttributeRight
                                                                                  relatedBy:NSLayoutRelationEqual
                                                                                     toItem:self.view
                                                                                  attribute:NSLayoutAttributeRight
                                                                                 multiplier:1.0
                                                                                   constant:0];
    
    [self.view addConstraints:@[ ecHeightConstraint,
                                 ecWidthConstraint,
                                 ecTopConstraint,
                                 ecBottomConstraint,
                                 ecLeftConstraint,
                                 ecRightConstraint]];
    
    UILabel *headerContent = [[UILabel alloc] init];
    headerContent.translatesAutoresizingMaskIntoConstraints = NO;
    //headerContent.backgroundColor = [UIColor greenColor];
    headerContent.textAlignment = NSTextAlignmentCenter;
    headerContent.textColor = [UIColor grayColor];
    headerContent.font = [UIFont systemFontOfSize:20];
    headerContent.text = @"Start scanning for:";
    [self.view addSubview:headerContent];
    
    NSLayoutConstraint *headerContentXConstraint = [NSLayoutConstraint
                                                    constraintWithItem:headerContent attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:self.view attribute:
                                                    NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    
    NSLayoutConstraint *headerContentHeight = [NSLayoutConstraint constraintWithItem:headerContent
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1.0
                                                                       constant:40];

    NSLayoutConstraint *headerContentYConstraint = [NSLayoutConstraint
                                                    constraintWithItem:headerContent attribute:NSLayoutAttributeCenterY
                                                    relatedBy:NSLayoutRelationEqual toItem:self.view attribute:
                                                    NSLayoutAttributeCenterY multiplier:1.0f constant:(-headerContent.frame.size.height / 2) + 20];
    
    NSLayoutConstraint *headerContentConstraintLeft = [NSLayoutConstraint constraintWithItem:headerContent
                                                                              attribute:NSLayoutAttributeLeft
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:self.view
                                                                              attribute:NSLayoutAttributeLeft
                                                                             multiplier:1.0
                                                                               constant:40];
    
    NSLayoutConstraint *headerContentConstraintRight = [NSLayoutConstraint constraintWithItem:headerContent
                                                                               attribute:NSLayoutAttributeRight
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:self.view
                                                                               attribute:NSLayoutAttributeRight
                                                                              multiplier:1.0
                                                                                constant:-40];
    
    [self.view addConstraints:@[ headerContentXConstraint,
                                 headerContentHeight,
                                 headerContentYConstraint,
                                 headerContentConstraintLeft,
                                 headerContentConstraintRight]];
    */
    
    /*
     * Init Scan Button
     */
    
    UIButton *scanButton = [UIButton buttonWithType:UIButtonTypeSystem];
    scanButton.frame = CGRectMake(40, _screenHeight - 70, _screenWidth - 80, 50);
    scanButton.layer.cornerRadius = 10;
    [scanButton setTitle:@"SCAN QR CODE" forState:UIControlStateNormal];
    [scanButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scanButton setBackgroundColor:[UIColor colorWithRed: 23.0f/255.0f
                                                   green: 95.0f/255.0f
                                                    blue: 174.0f/255.0f
                                                   alpha: 1]];
    [self.view addSubview:scanButton];
    
    [scanButton addTarget:self action:@selector(courseStartScan) forControlEvents:UIControlEventTouchUpInside];
    
    /*
     * Make container for event details
     */

    UIView *eventContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, eventImageView.frame.origin.y + eventImageView.frame.size.height, _screenWidth, _screenHeight - (eventImageView.frame.origin.y + eventImageView.frame.size.height + (_screenHeight - scanButton.frame.origin.y)))];
    //eventContainerView.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:eventContainerView];
    
    /*
     * Margin for labels
     */
    
    float textSideMargin = 40;
    
    /*
     * Date of event
     */
    
    UILabel *date = [[UILabel alloc] init];
    //date.backgroundColor = [UIColor purpleColor];
    date.translatesAutoresizingMaskIntoConstraints = NO;
    date.textColor = [UIColor blackColor];
    date.textAlignment = NSTextAlignmentCenter;
    date.text = _courseDate;
    date.font = [UIFont fontWithName:@"Roboto-Thin" size:15];
    [eventContainerView addSubview:date];
    
    NSLayoutConstraint *dateYConstraint = [NSLayoutConstraint
                                           constraintWithItem: date
                                                    attribute: NSLayoutAttributeCenterY
                                                    relatedBy: NSLayoutRelationEqual
                                                       toItem: eventContainerView
                                                    attribute: NSLayoutAttributeCenterY
                                                   multiplier: 1.0f
                                                     constant: 0];
    
    NSLayoutConstraint *dateHeightConstraint = [NSLayoutConstraint
                                                constraintWithItem: date
                                                         attribute: NSLayoutAttributeHeight
                                                         relatedBy: NSLayoutRelationEqual
                                                            toItem: nil
                                                         attribute: NSLayoutAttributeNotAnAttribute
                                                        multiplier: 1.0
                                                          constant: 30];
    
    NSLayoutConstraint *dateLeftConstraint = [NSLayoutConstraint
                                              constraintWithItem: date
                                                       attribute: NSLayoutAttributeLeft
                                                       relatedBy: NSLayoutRelationEqual
                                                          toItem: eventContainerView
                                                       attribute: NSLayoutAttributeLeft
                                                      multiplier: 1.0
                                                        constant: textSideMargin];
    
    NSLayoutConstraint *dateRightConstraint = [NSLayoutConstraint
                                               constraintWithItem: date
                                                        attribute: NSLayoutAttributeRight
                                                        relatedBy: NSLayoutRelationEqual
                                                           toItem: eventContainerView
                                                        attribute: NSLayoutAttributeRight
                                                       multiplier: 1.0
                                                         constant: -textSideMargin];

    [eventContainerView addConstraints:@[ dateYConstraint,
                                          dateHeightConstraint,
                                          dateLeftConstraint,
                                          dateRightConstraint]];
    
    UILabel *title = [[UILabel alloc] init];
    //title.backgroundColor = [UIColor greenColor];
    title.translatesAutoresizingMaskIntoConstraints = NO;
    title.textColor = [UIColor blackColor];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = _courseTitle;
    title.numberOfLines = 0;
    title.font = [UIFont fontWithName:@"Roboto-Light" size:20];
    [eventContainerView addSubview:title];
    
    NSLayoutConstraint *titleYConstraint = [NSLayoutConstraint
                                            constraintWithItem: title
                                                     attribute: NSLayoutAttributeBottom
                                                     relatedBy: NSLayoutRelationEqual
                                                        toItem: date
                                                     attribute: NSLayoutAttributeTop
                                                    multiplier: 1.0f
                                                      constant: 0];
    
    NSLayoutConstraint *titleHeightConstraint = [NSLayoutConstraint
                                                 constraintWithItem: title
                                                          attribute: NSLayoutAttributeHeight
                                                          relatedBy: NSLayoutRelationGreaterThanOrEqual
                                                             toItem: nil
                                                          attribute: NSLayoutAttributeNotAnAttribute
                                                         multiplier: 1.0
                                                           constant: 50];
    
    NSLayoutConstraint *titleLeftConstraint = [NSLayoutConstraint
                                            constraintWithItem: title
                                                     attribute: NSLayoutAttributeLeft
                                                     relatedBy: NSLayoutRelationEqual
                                                        toItem: eventContainerView
                                                     attribute: NSLayoutAttributeLeft
                                                    multiplier: 1.0
                                                      constant: textSideMargin];
    
    NSLayoutConstraint *titleRightConstraint = [NSLayoutConstraint
                                             constraintWithItem: title
                                                      attribute: NSLayoutAttributeRight
                                                      relatedBy: NSLayoutRelationEqual
                                                         toItem: eventContainerView
                                                      attribute: NSLayoutAttributeRight
                                                     multiplier: 1.0
                                                       constant: -textSideMargin];
    
    [eventContainerView addConstraints:@[ titleYConstraint,
                                          titleHeightConstraint,
                                          titleLeftConstraint,
                                          titleRightConstraint]];
    
    UILabel *time = [[UILabel alloc] init];
    //time.backgroundColor = [UIColor yellowColor];
    time.translatesAutoresizingMaskIntoConstraints = NO;
    time.textColor = [UIColor blackColor];
    time.textAlignment = NSTextAlignmentCenter;
    time.text = _courseTime;
    time.font = [UIFont fontWithName:@"Roboto-Thin" size:15];
    [eventContainerView addSubview:time];
    
    NSLayoutConstraint *timeYConstraint = [NSLayoutConstraint
                                           constraintWithItem:time
                                           attribute:NSLayoutAttributeTop
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:date
                                           attribute:NSLayoutAttributeBottom
                                           multiplier:1.0f
                                           constant:0];
    
    NSLayoutConstraint *timeHeightConstraint = [NSLayoutConstraint
                                                constraintWithItem:time
                                                attribute:NSLayoutAttributeHeight
                                                relatedBy:NSLayoutRelationEqual
                                                toItem:nil
                                                attribute:NSLayoutAttributeNotAnAttribute
                                                multiplier:1.0
                                                constant:30];
    
    NSLayoutConstraint *timeLeftConstraint = [NSLayoutConstraint
                                              constraintWithItem:time
                                              attribute:NSLayoutAttributeLeft
                                              relatedBy:NSLayoutRelationEqual
                                              toItem:eventContainerView
                                              attribute:NSLayoutAttributeLeft
                                              multiplier:1.0
                                              constant:textSideMargin];
    
    NSLayoutConstraint *timeRightConstraint = [NSLayoutConstraint
                                               constraintWithItem:time
                                               attribute:NSLayoutAttributeRight
                                               relatedBy:NSLayoutRelationEqual
                                               toItem:eventContainerView
                                               attribute:NSLayoutAttributeRight
                                               multiplier:1.0
                                               constant:-textSideMargin];

    [eventContainerView addConstraints:@[ timeYConstraint,
                                          timeHeightConstraint,
                                          timeLeftConstraint,
                                          timeRightConstraint]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * ================ Protocol : Delegate ================
 */

-(void)courseStartScan{
    
    /*
     * We are passing or letting it to access this method to the 
     * SwitchViewController who handle this view controller (CourseViewController)
     * and other view controller (AttendanceViewController)
     *
     * SwitchViewController act as the main controller for both
     * CourseViewController and AttendanceViewController
     */
    
    [self.delegate courseStartScan];
}
@end
