//
//  SettingsBar.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 5/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "SettingsBar.h"

//#import "AssetsModification.h" disable

@implementation SettingsBar

-(id)initWithTarget:(UIViewController*)vc{
    self = [super init];
    
    if(self){
        
        /*
         * Stop to poping the actionsheet at init
         */
        
        if(_targetViewController != nil){
            [self settingsBarShowSettingsMenu];
        }
        
        /*
         * This target or appoint to the current view
         */
        
        _targetViewController = vc;
        
        /*
         * selfDelegate is for iOS 7 version only
         * this prevent from crashing because it has different actionsheets method than iOS 8 and above
         */
        _selfDelegate = self;
    }
    return self;
}

-(void)addSettingsBarImage:(NSString*)imageName{
    /*
     * This create a custom button with image without pixilating the image
     */
    UIButton *settingsButton = [[UIButton alloc] init];
    settingsButton.frame = CGRectMake(0, 0, _targetViewController.navigationController.navigationBar.frame.size.height - 15, _targetViewController.navigationController.navigationBar.frame.size.height - 15);
    [settingsButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    settingsButton.contentMode = UIViewContentModeScaleAspectFit;
    settingsButton.clipsToBounds = YES;
    [settingsButton addTarget:self action:@selector(settingsBarShowSettingsMenu) forControlEvents:UIControlEventTouchUpInside];

    /*
     * Add "Option" button in navigation bar at top right of the screen
     */
    
    UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc] initWithCustomView:settingsButton];
    
    /*
     * Old top right button for navigation
     * using this will pixilate the image if using "AssetsModification" class
     * without using "AssetsModification" class you to manually resize the image but not recommended
     *
     
    AssetsModification *am = [[AssetsModification alloc] init];
     
    UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc]
                                          initWithImage:[am imageResizeByCustom: imageName rectSize:CGSizeMake(_targetViewController.navigationController.navigationBar.frame.size.height - 4, _targetViewController.navigationController.navigationBar.frame.size.height - 4)]
                                          style:UIBarButtonItemStylePlain
                                          target:_selfDelegate
                                          action:@selector(settingsBarShowSettingsMenu)];
     
     UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc] initWithCustomView:settingsButton];
    */
    
    _targetViewController.navigationItem.rightBarButtonItem = settingsBarButton;
}

-(void)settingsBarShowSettingsMenu{
    if (NSClassFromString(@"UIAlertController") != nil) {
        /*
         * For iOS +8 version only
         */
        
        //NSLog(@"UIAlertController can be instantiated");
        
        [self showActionSheet];
    }else{
        /*
         * For iOS 7 version only
         */
        
        //NSLog(@"UIAlertController cannot be instantiated");

        UIActionSheet *_actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                  delegate:_selfDelegate
                                                         cancelButtonTitle:@"Cancel"
                                                    destructiveButtonTitle:nil
                                                         otherButtonTitles: @"Help / FAQ",
                                       @"Logout",
                                       nil];

        /*
         * This prevent the iOS 7 from crashing because it doesnt recognize which window
         * Need this for old actionsheet method
         */
        
        UIWindow* window = [[[UIApplication sharedApplication] delegate] window];

        if ([window.subviews containsObject:_targetViewController.view]) {
            [_actionSheet showInView:_targetViewController.view];
        } else {
            [_actionSheet showInView:window];
        }
    }
}

-(void)showActionSheet{
    
    /*
     * Instantiate view for action sheet
     */
    
    UIAlertController *actionView = [UIAlertController
                               alertControllerWithTitle:nil
                               message:nil
                               preferredStyle:UIAlertControllerStyleActionSheet];
    
    /*
     * List of option of action sheet
     *
     * Help / FAQ
     * Logout
     * Cancel
     */
    
    UIAlertAction* ac1 = [UIAlertAction
                          actionWithTitle:@"Help / FAQ"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              [actionView dismissViewControllerAnimated:YES completion:nil];
                              
                          }];
    UIAlertAction* ac2 = [UIAlertAction
                          actionWithTitle:@"Logout"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              [actionView dismissViewControllerAnimated:YES completion:nil];
                              
                              [_targetViewController.navigationController popToRootViewControllerAnimated:YES];
                              
                          }];
    UIAlertAction* ac3 = [UIAlertAction
                          actionWithTitle:@"Cancel"
                          style:UIAlertActionStyleCancel
                          handler:^(UIAlertAction * action)
                          {
                              [actionView dismissViewControllerAnimated:YES completion:nil];
                              
                          }];
    
    /*
     * Added them all in action controller
     */
    
    [actionView addAction:ac1];
    [actionView addAction:ac2];
    [actionView addAction:ac3];
    [_targetViewController presentViewController:actionView animated:YES completion:nil];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        [_targetViewController.navigationController popToRootViewControllerAnimated:YES];
    }
}
@end
