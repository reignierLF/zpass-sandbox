//
//  StyleBoxView.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 28/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//
/*
 * Custom Cell for scrollview and not for TableViewController
 * just add custom frame or base on scrollview content size
 */

#import "StyleBoxCellView.h"

@implementation StyleBoxCellView
/*
 * This is a stylish Cell
 *
 
-(id)initWithFrame:(CGRect)frame image:(NSString*)imageName{
    self = [super initWithFrame:frame];
    
    if(self){
        float styleBoxWidth = frame.size.width;
        float styleBoxHeight = frame.size.height;
        
        UIImageView *eventImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        //UIImageView *eventImageView = [[UIImageView alloc] init];
        eventImageView.frame = CGRectMake(0, 10, frame.size.width, frame.size.height - 10);
        eventImageView.contentMode = UIViewContentModeScaleAspectFill;
        eventImageView.alpha = 0.8;
        eventImageView.clipsToBounds = YES;
        
        _image = eventImageView.image;
        
        //eventImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:eventImageView];
        
        CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color1 = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        
        CGFloat hue2 = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation2 = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness2 = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color2 = [UIColor colorWithHue:hue2 saturation:saturation2 brightness:brightness2 alpha:1];

        UIView *styleBox1 = [[UIView alloc] init];
        styleBox1.backgroundColor = color1;
        styleBox1.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:styleBox1];
        
        NSLayoutConstraint *styleBox1PaddingLeft = [NSLayoutConstraint
                                                        constraintWithItem:styleBox1 attribute:NSLayoutAttributeLeft
                                                        relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                        NSLayoutAttributeLeft multiplier:1.0 constant:20];
        
        NSLayoutConstraint *styleBox1PaddingRight = [NSLayoutConstraint
                                                    constraintWithItem:styleBox1 attribute:NSLayoutAttributeRight
                                                    relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                    NSLayoutAttributeRight multiplier:1.0 constant:-(styleBoxWidth / 4)];
        
        NSLayoutConstraint *styeBox1Top = [NSLayoutConstraint constraintWithItem:styleBox1
                                                                          attribute:NSLayoutAttributeTop
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self
                                                                          attribute:NSLayoutAttributeTop
                                                                         multiplier:1.0
                                                                           constant:20];
        
        NSLayoutConstraint *styeBox1Bottom = [NSLayoutConstraint constraintWithItem:styleBox1
                                                                       attribute:NSLayoutAttributeBottom
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeBottom
                                                                      multiplier:1.0
                                                                        constant:-(styleBoxHeight / 3)];
        

        [self addConstraints:@[ styleBox1PaddingLeft,
                                    styleBox1PaddingRight,
                                    styeBox1Top,
                                    styeBox1Bottom]];
        
        UIView *styleBox2 = [[UIView alloc] init];
        styleBox2.translatesAutoresizingMaskIntoConstraints = NO;
        styleBox2.backgroundColor = color2;
        [self addSubview:styleBox2];
        
        NSLayoutConstraint *styleBox2PaddingLeft = [NSLayoutConstraint
                                                    constraintWithItem:styleBox2 attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                    NSLayoutAttributeLeft multiplier:1.0 constant:(styleBoxWidth / 4)];
        
        NSLayoutConstraint *styleBox2PaddingRight = [NSLayoutConstraint
                                                     constraintWithItem:styleBox2 attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                     NSLayoutAttributeRight multiplier:1.0 constant:-20];
        
        NSLayoutConstraint *styeBox2Top = [NSLayoutConstraint constraintWithItem:styleBox2
                                                                       attribute:NSLayoutAttributeTop
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeTop
                                                                      multiplier:1.0
                                                                        constant:30];
        
        NSLayoutConstraint *styeBox2Bottom = [NSLayoutConstraint constraintWithItem:styleBox2
                                                                          attribute:NSLayoutAttributeBottom
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self
                                                                          attribute:NSLayoutAttributeBottom
                                                                         multiplier:1.0
                                                                           constant:-((styleBoxHeight / 3) - 10)];
        
        
        [self addConstraints:@[ styleBox2PaddingLeft,
                                styleBox2PaddingRight,
                                styeBox2Top,
                                styeBox2Bottom]];
        
        //UIView *detailView = [[UIView alloc] initWithFrame:CGRectMake(styleBox1.frame.origin.x + 20, styleBox2.frame.origin.y + 20, styleBoxWidth - (styleBoxWidth / 4), styleBoxHeight - (styleBoxHeight / 2))];
        UIView *detailView = [[UIView alloc] init];
        detailView.translatesAutoresizingMaskIntoConstraints = NO;
        detailView.backgroundColor = [UIColor whiteColor];
        [self addSubview:detailView];
        
        NSLayoutConstraint *detailViewPaddingLeft = [NSLayoutConstraint
                                                    constraintWithItem:detailView attribute:NSLayoutAttributeLeft
                                                    relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                    NSLayoutAttributeLeft multiplier:1.0 constant:40];
        
        NSLayoutConstraint *detailViewPaddingRight = [NSLayoutConstraint
                                                     constraintWithItem:detailView attribute:NSLayoutAttributeRight
                                                     relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                     NSLayoutAttributeRight multiplier:1.0 constant:-40];
        
        NSLayoutConstraint *detailViewTop = [NSLayoutConstraint constraintWithItem:detailView
                                                                       attribute:NSLayoutAttributeTop
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:self
                                                                       attribute:NSLayoutAttributeTop
                                                                      multiplier:1.0
                                                                        constant:40];
        
        NSLayoutConstraint *detailViewBottom = [NSLayoutConstraint constraintWithItem:detailView
                                                                          attribute:NSLayoutAttributeBottom
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self
                                                                          attribute:NSLayoutAttributeBottom
                                                                         multiplier:1.0
                                                                           constant:-20];
        
        
        [self addConstraints:@[ detailViewPaddingLeft,
                                detailViewPaddingRight,
                                detailViewTop,
                                detailViewBottom]];
        
        UILabel *label = [[UILabel alloc] init];
        label.backgroundColor = [UIColor lightTextColor];
        label.textColor = [UIColor grayColor];
        label.font = [UIFont systemFontOfSize:20];
        //label.textAlignment = NSTextAlignmentCenter;
        label.translatesAutoresizingMaskIntoConstraints = NO;
        label.text = @"   4:00 PM - 12:00 PM";
        [self addSubview:label];
        
        NSLayoutConstraint *labeladdingLeft = [NSLayoutConstraint
                                                     constraintWithItem:label attribute:NSLayoutAttributeLeft
                                                     relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                     NSLayoutAttributeLeft multiplier:1.0 constant:40];
        
        NSLayoutConstraint *labelPaddingRight = [NSLayoutConstraint
                                                      constraintWithItem:label attribute:NSLayoutAttributeRight
                                                      relatedBy:NSLayoutRelationEqual toItem:self attribute:
                                                      NSLayoutAttributeRight multiplier:1.0 constant:-40];
        
        NSLayoutConstraint *labelTop = [NSLayoutConstraint constraintWithItem:label
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1.0
                                                                          constant:40];
        
        NSLayoutConstraint *labelBottom = [NSLayoutConstraint constraintWithItem:label
                                                                            attribute:NSLayoutAttributeBottom
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self
                                                                            attribute:NSLayoutAttributeBottom
                                                                           multiplier:1.0
                                                                             constant:-20];
        
        [self addConstraints:@[ labeladdingLeft,
                                labelPaddingRight,
                                labelTop,
                                labelBottom]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(0, 0, styleBoxWidth, styleBoxHeight - 10);
        //button.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
        [self addSubview:button];
        
        [button addTarget:self action:@selector(StyleBoxCellViewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}
*/
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

/*
 * Init plain image background and a rounded box for detail
 */

-(id)initWithFrame:(CGRect)frame image:(NSString*)imageName{
    self = [super initWithFrame:frame];
    
    if(self){
        
        /*
         * For short-cut call of frame size
         */
        
        const float styleBoxWidth = frame.size.width;
        const float styleBoxHeight = frame.size.height;
        
        /*
         * This will make the StyleBoxCellView default background
         */
        
        UIImageView *eventImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        eventImageView.frame = CGRectMake(0, 0, styleBoxWidth, styleBoxHeight - 2);
        eventImageView.contentMode = UIViewContentModeScaleAspectFill;
        eventImageView.alpha = 0.8;
        eventImageView.clipsToBounds = YES;
        [self addSubview:eventImageView];
        
        /*
         * For reading image only
         */
        
        _image = eventImageView.image;
        
        /*
         * Place holder for details of event
         * Rounded box with semi transparent white background
         */
        
        UIView *detailView = [[UIView alloc] init];
        detailView.translatesAutoresizingMaskIntoConstraints = NO;
        detailView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
        detailView.layer.cornerRadius = 10;
        [self addSubview:detailView];
        
        /*
         * Stretch the detailView to the top of the screen
         * with a margin value of 20px
         */
        
        NSLayoutConstraint *detailViewTopConstraint = [NSLayoutConstraint
                                                       constraintWithItem:detailView
                                                       attribute:NSLayoutAttributeTop
                                                       relatedBy:NSLayoutRelationEqual
                                                       toItem:self
                                                       attribute:NSLayoutAttributeTop
                                                       multiplier:1.0
                                                       constant:20];
        
        /*
         * Stretch the detailView to the bottom of the screen
         * with a margin value of 20px
         */
        
        NSLayoutConstraint *detailViewBottomConstraint = [NSLayoutConstraint
                                                          constraintWithItem:detailView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                          toItem:self
                                                          attribute:NSLayoutAttributeBottom
                                                          multiplier:1.0
                                                          constant:-20];
        
        /*
         * Stretch the detailView to the left side of the screen
         * with a margin value of 20px
         */
        
        NSLayoutConstraint *detailViewLeftConstraint = [NSLayoutConstraint
                                                        constraintWithItem:detailView
                                                        attribute:NSLayoutAttributeLeft
                                                        relatedBy:NSLayoutRelationEqual
                                                        toItem:self
                                                        attribute:NSLayoutAttributeLeft
                                                        multiplier:1.0
                                                        constant:20];
        
        /*
         * Stretch the detailView to the right side of the screen
         * with a margin value of 20px
         */
        
        NSLayoutConstraint *detailViewRightConstraint = [NSLayoutConstraint
                                                         constraintWithItem:detailView
                                                         attribute:NSLayoutAttributeRight
                                                         relatedBy:NSLayoutRelationEqual
                                                         toItem:self
                                                         attribute:NSLayoutAttributeRight
                                                         multiplier:1.0
                                                         constant:-20];
        
        /*
         * Compile/Add all Constraints for detailView
         */
        
        [self addConstraints:@[ detailViewTopConstraint,
                                detailViewBottomConstraint,
                                detailViewLeftConstraint,
                                detailViewRightConstraint]];
        
        /*
         * Value for margin or gap of labels
         * date, title and time
         */
        
        const float textSideMargin = 40;
        
        /*
         * Instantiate date of event
         */
        
        _date = [[UILabel alloc] init];
        //_date.backgroundColor = [UIColor purpleColor];
        _date.translatesAutoresizingMaskIntoConstraints = NO;
        _date.textColor = [UIColor blackColor];
        _date.textAlignment = NSTextAlignmentCenter;
        _date.text = @"Date";
        [self addSubview:_date];
        
        /*
         * Put the date(UILabel) in the center of Y-axis
         */
        
        NSLayoutConstraint *dateYConstraint = [NSLayoutConstraint
                                               constraintWithItem:_date
                                               attribute:NSLayoutAttributeCenterY
                                               relatedBy:NSLayoutRelationEqual
                                               toItem:self
                                               attribute:NSLayoutAttributeCenterY
                                               multiplier:1.0f
                                               constant:10];
        
        /*
         * Make height for date(UILabel)
         */
        
        NSLayoutConstraint *dateHeightConstraint = [NSLayoutConstraint
                                                    constraintWithItem:_date
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.0
                                                    constant:20];
        
        /*
         * Stretch the date(UILabel) to the left side of the screen
         * with a margin var of textSideMargin(40px)
         */
        
        NSLayoutConstraint *dateLeftConstraint = [NSLayoutConstraint
                                                  constraintWithItem:_date
                                                  attribute:NSLayoutAttributeLeft
                                                  relatedBy:NSLayoutRelationEqual
                                                  toItem:self
                                                  attribute:NSLayoutAttributeLeft
                                                  multiplier:1.0
                                                  constant:textSideMargin];
        
        /*
         * Stretch the date(UILabel) to the right side of the screen
         * with a margin var of textSideMargin(40px)
         */
        
        NSLayoutConstraint *dateRightConstraint = [NSLayoutConstraint
                                                   constraintWithItem:_date
                                                   attribute:NSLayoutAttributeRight
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:self
                                                   attribute:NSLayoutAttributeRight
                                                   multiplier:1.0
                                                   constant:-textSideMargin];
        
        /*
         * Compile/Add all Constraints for date(UILabel)
         */
        
        [self addConstraints:@[ dateYConstraint,
                                dateHeightConstraint,
                                dateLeftConstraint,
                                dateRightConstraint]];
        
        /*
         * Instantiate title of event
         */
        
        _title = [[UILabel alloc] init];
        //_title.backgroundColor = [UIColor greenColor];
        _title.translatesAutoresizingMaskIntoConstraints = NO;
        _title.textColor = [UIColor blackColor];
        _title.textAlignment = NSTextAlignmentCenter;
        _title.text = @"Title";
        _title.numberOfLines = 0;
        [self addSubview:_title];
        
        /*
         * Align title's(UILabel) bottom to the top of date(UILabel)
         */
        
        NSLayoutConstraint *titleYConstraint = [NSLayoutConstraint
                                                constraintWithItem:_title
                                                attribute:NSLayoutAttributeBottom
                                                relatedBy:NSLayoutRelationEqual
                                                toItem:_date
                                                attribute:NSLayoutAttributeTop
                                                multiplier:1.0f
                                                constant:0];
        
        /*
         * Make height for title(UILabel)
         */
        
        NSLayoutConstraint *titleHeightConstraint = [NSLayoutConstraint
                                                     constraintWithItem:_title
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                     toItem:nil
                                                     attribute:NSLayoutAttributeNotAnAttribute
                                                     multiplier:1.0
                                                     constant:40];
        
        /*
         * Stretch the title(UILabel) to the left side of the screen
         * with a margin var of textSideMargin(40px)
         */
        
        NSLayoutConstraint *titleLeftConstraint = [NSLayoutConstraint
                                                   constraintWithItem:_title
                                                   attribute:NSLayoutAttributeLeft
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:self
                                                   attribute:NSLayoutAttributeLeft
                                                   multiplier:1.0
                                                   constant:textSideMargin];
        
        /*
         * Stretch the title(UILabel) to the right side of the screen
         * with a margin var of textSideMargin(40px)
         */
        
        NSLayoutConstraint *titleRightConstraint = [NSLayoutConstraint
                                                    constraintWithItem:_title
                                                    attribute:NSLayoutAttributeRight
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:self
                                                    attribute:NSLayoutAttributeRight
                                                    multiplier:1.0
                                                    constant:-textSideMargin];
        
        /*
         * Compile/Add all Constraints for title(UILabel)
         */
        
        [self addConstraints:@[ titleYConstraint,
                                titleHeightConstraint,
                                titleLeftConstraint,
                                titleRightConstraint]];
        
        /*
         * Instantiate time of event
         */
        
        _time = [[UILabel alloc] init];
        //_time.backgroundColor = [UIColor yellowColor];
        _time.translatesAutoresizingMaskIntoConstraints = NO;
        _time.textColor = [UIColor blackColor];
        _time.textAlignment = NSTextAlignmentCenter;
        _time.text = @"Time";
        [self addSubview:_time];
        
        /*
         * Align time's(UILabel) top to the bottom of date(UILabel)
         */
        
        NSLayoutConstraint *timeYConstraint = [NSLayoutConstraint
                                               constraintWithItem:_time
                                               attribute:NSLayoutAttributeTop
                                               relatedBy:NSLayoutRelationEqual
                                               toItem:_date
                                               attribute:NSLayoutAttributeBottom
                                               multiplier:1.0f
                                               constant:0];
        
        /*
         * Make height for time(UILabel)
         */
        
        NSLayoutConstraint *timeHeightConstraint = [NSLayoutConstraint
                                                    constraintWithItem:_time
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:nil
                                                    attribute:NSLayoutAttributeNotAnAttribute
                                                    multiplier:1.0
                                                    constant:20];
        
        /*
         * Stretch the time(UILabel) to the left side of the screen
         * with a margin var of textSideMargin(40px)
         */
        
        NSLayoutConstraint *timeLeftConstraint = [NSLayoutConstraint
                                                  constraintWithItem:_time
                                                  attribute:NSLayoutAttributeLeft
                                                  relatedBy:NSLayoutRelationEqual
                                                  toItem:self
                                                  attribute:NSLayoutAttributeLeft
                                                  multiplier:1.0
                                                  constant:textSideMargin];
        
        /*
         * Stretch the time(UILabel) to the right side of the screen
         * with a margin var of textSideMargin(40px)
         */
        
        NSLayoutConstraint *timeRightConstraint = [NSLayoutConstraint
                                                   constraintWithItem:_time
                                                   attribute:NSLayoutAttributeRight
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:self
                                                   attribute:NSLayoutAttributeRight
                                                   multiplier:1.0
                                                   constant:-textSideMargin];
        
        /*
         * Compile/Add all Constraints for time(UILabel)
         */
        
        [self addConstraints:@[ timeYConstraint,
                                timeHeightConstraint,
                                timeLeftConstraint,
                                timeRightConstraint]];
        
        /*
         * Act as the button of the whole cell
         */
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(0, 0, styleBoxWidth, eventImageView.frame.size.height);
        //button.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.2];
        [self addSubview:button];
        
        [button addTarget:self action:@selector(styleBoxCellViewButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
}

/*
 * ================ Protocol : Delegate ================
 */

-(void)styleBoxCellViewButtonPressed:(UIButton*)sender{
    
    /*
     * When the cell is pressed, we are passing these vars/data
     * to the view controller parent
     *
     * Note
     * data will be pass : image, title, date and time
     */
    
    [self.delegate styleBoxCellViewButtonPressed:self.tag image:_image title:_title.text date:_date.text time:_time.text];
}
@end
