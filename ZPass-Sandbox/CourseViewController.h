//
//  CourseViewController.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 23/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 * Protocol for delegate
 * Add methods if needed
 */

@protocol CourseDelegate
- (void)courseStartScan;
@end

@interface CourseViewController : UIViewController

/*
 * This global vars
 * Used for getting/access data from other controller
 */

@property (nonatomic, assign) float screenYOffSet;
@property (nonatomic, assign) float screenWidth;
@property (nonatomic, assign) float screenHeight;
@property (nonatomic, assign) UIImage *image;
@property (nonatomic, assign) NSString *courseTitle;
@property (nonatomic, assign) NSString *courseDate;
@property (nonatomic, assign) NSString *courseTime;

/*
 * Custom delegate
 */

@property (nonatomic, weak) id <CourseDelegate> delegate;

@end
