//
//  Events.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 30/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"

@interface Events : NSObject

@property (nonatomic, strong) Event *event;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
