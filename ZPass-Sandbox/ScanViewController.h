//
//  ScanViewController.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 23/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ScanViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate, UIScrollViewDelegate>

@property (nonatomic, assign) float screenYOffSet;

@property (nonatomic, readonly) NSMutableArray *dataLogsMutableArray;

@end
