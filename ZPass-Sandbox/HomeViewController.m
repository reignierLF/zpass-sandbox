//
//  HomeViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 28/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//
#import "cUIScrollView.h"

#import "HomeViewController.h"
#import "SwitchViewController.h"
#import "SettingsBar.h"

@interface HomeViewController ()

@property (nonatomic) float screenYOffSet;
@property (nonatomic) float screenWidth;
@property (nonatomic) float screenHeight;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /*
     * Unhide the navigation bar once inside Home screen
     */
    
    self.navigationController.navigationBarHidden = NO;
    
    /*
     * Hide the back button, since we dont want to
     * return the login screen
     * Use the logout button at Settings button (gear wheel icon)
     */
    
    self.navigationItem.hidesBackButton = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    /*
     * screenYOffset : Get the height of navigation bar to be use to offset the
     *                 screen's Y-axis position and +20 value is for status bar height
     *
     * screenWidth : Get the screen width so we dont have to type this
     *               long word (self.view.frame.size.width)
     *
     * screenHeight : Get the screen height and subtract the height of
     *                navigation bar
     */
    
    _screenYOffSet = self.navigationController.navigationBar.frame.size.height + 20;
    _screenWidth = self.view.frame.size.width;
    _screenHeight = self.view.frame.size.height - _screenYOffSet;
    
    /*
     * Instantiate custom setting bar appear at top-right
     * of the screen (gear wheel icon)
     */
    
    SettingsBar *sb = [[SettingsBar alloc] initWithTarget:self];
    
    /*
     * This method is required only a image
     * and it automatically place in the navigation bar
     * at the top-right of the screen
     *
     * NOTE
     * Check the SettingBar Class if needed
     */
    
    [sb addSettingsBarImage:@"settings-ios"];
    
    /*
     * OPTION BUTTON 
     * Add custom "Option" button without navigation bar or if its hidden
     *
     
    UIButton *optionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    optionButton.frame = CGRectMake(screenWidth - 35, 30, 30, 30);
    [optionButton setImage:[UIImage imageNamed:@"settings-ios"] forState:UIControlStateNormal];
    [self.view addSubview:optionButton];
    
    [optionButton addTarget:self action:@selector(optionEvent) forControlEvents:UIControlEventTouchUpInside];
    */
    
    /*
     * Set Background image
     *
     
    UIImageView *eventImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zpass splash 750 copy"]];
    eventImageView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    eventImageView.contentMode = UIViewContentModeScaleAspectFill;
    eventImageView.clipsToBounds = YES;
    
    [self.view insertSubview:eventImageView atIndex:0];
    */
    
    /*
     * Got from the json response how many events received
     */
    
    int numberOfScheduleEvents = 5;
    
    /*
     * Note
     * cUIScrollView is a subclass/custom class
     * check its detail from cUIScrollView class if needed
     *
     * Instantiate and act as list of events
     */
    
    cUIScrollView *scheduleListEventsScrollView = [[cUIScrollView alloc] initWithFrame:CGRectMake(0, _screenYOffSet, _screenWidth, _screenHeight)];
    scheduleListEventsScrollView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    scheduleListEventsScrollView.contentSize = CGSizeMake(scheduleListEventsScrollView.frame.size.width, -_screenYOffSet + ((_screenHeight / 3) * numberOfScheduleEvents));
    scheduleListEventsScrollView.delaysContentTouches = NO;
    [self.view addSubview:scheduleListEventsScrollView];
    
    /*
     * Note
     * StyleBoxCellView is a custom class and act as CellView
     * similar to UITableViewCell
     * check its detail from StyleBoxCellView class if needed
     */
    
    StyleBoxCellView *scheduleEventSlot;
    
    /*
     * Create a array container for scheduleEventSlot
     * this is used for animation effect
     */
    
    NSMutableArray *scheduleSlotArray = [[NSMutableArray alloc] init];
    
    /*
     * Populate the cells with data such as title, time, image..etc
     * base on how many events are there
     */

    for (int i = 0; i < numberOfScheduleEvents; i++) {
        scheduleEventSlot = [[StyleBoxCellView alloc] initWithFrame:CGRectMake(-(_screenWidth / 2), -_screenYOffSet + ((_screenHeight / 3) * i), _screenWidth, (_screenHeight / 3))image:[NSString stringWithFormat:@"natureBG%i",i + 1]];
        scheduleEventSlot.delegate = self;
        
        scheduleEventSlot.title.text = @"Golden Hour Photography";
        scheduleEventSlot.title.font = [UIFont fontWithName:@"Roboto-Light" size:18];
        
        scheduleEventSlot.date.text = @"DATE 12 12 12";
        scheduleEventSlot.date.font = [UIFont fontWithName:@"Roboto-Thin" size:14];
        
        scheduleEventSlot.time.text = @"1:11PM - 11:11PM";
        scheduleEventSlot.time.font = [UIFont fontWithName:@"Roboto-Thin" size:14];
        
        scheduleEventSlot.alpha = 0;
        scheduleEventSlot.tag = i;
        [scheduleListEventsScrollView addSubview:scheduleEventSlot];
        
        /*
         * Add the cell in array
         */
        
        [scheduleSlotArray addObject:scheduleEventSlot];
    }
    
    /*
     * Animation effect for each cells
     */
    
    for (int i = 0; i < numberOfScheduleEvents; i++) {
        
        [UIView animateWithDuration: 1
                              delay: i * 0.25
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations: ^{
                             
                             /*
                              * Animation blocks
                              * Set behaviour or style of the animation
                              */
                             
                             /*
                              * Get the frame of each cells
                              */
                             
                             CGRect frame = [[scheduleSlotArray objectAtIndex:i] frame];

                             /*
                              * Animation description
                              * Makes the cell appear similar to fade effect
                              * and move from outside left of the screen to inside
                              */
                             
                             [[scheduleSlotArray objectAtIndex:i] setAlpha:1];
                             [[scheduleSlotArray objectAtIndex:i] setFrame:CGRectMake(0, frame.origin.y, frame.size.width, frame.size.height)];
                             
                         }completion:^(BOOL finished){
                             if(finished){
                                 /*
                                  * Add to-do or anything after the animation 
                                  * finished animating
                                  */
                             }
                         }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * ==================== Delegates ====================
 */

/*
 * This delegates is from StyleBoxCellView Class
 */

-(void)styleBoxCellViewButtonPressed:(NSInteger)index image:(UIImage *)image title:(NSString *)title date:(NSString *)date time:(NSString *)time{
    
    /*
     * This will proceed to EventViewController after choosing/clicking any cells
     * pass value or anything needed to the next screen
     */
    
    SwitchViewController *swViewController = [[SwitchViewController alloc] init];
    swViewController.screenYOffSet = _screenYOffSet;
    swViewController.screenWidth = _screenWidth;
    swViewController.screenHeight = _screenHeight;
    swViewController.image = image;
    swViewController.courseTitle = title;
    swViewController.courseDate = date;
    swViewController.courseTime = time;
    [self.navigationController pushViewController:swViewController animated:YES];
    NSLog(@"Cell/Row : %li",(long)index);
}
@end
