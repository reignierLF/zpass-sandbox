//
//  AttendanceCellView.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 5/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttendanceCellView : UIView

/*
 * This global vars
 * Used for getting/access data from other controller
 *
 * Note
 * Just incase needed to read aswell as overwriting
 */

@property (nonatomic, strong, readwrite) UILabel *rowLabel;
@property (nonatomic, strong, readwrite) UILabel *fullNameLabel;

@end
