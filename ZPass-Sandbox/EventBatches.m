//
//  EventBatches.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 30/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "EventBatches.h"

@implementation EventBatches

-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){

        _EB_credit = [dictionary valueForKey:@"credit"];
        _EB_duration = [dictionary valueForKey:@"duration"];
        _EB_endTime = [dictionary valueForKey:@"end_time"];
        _EB_endedAt = [dictionary valueForKey:@"ended_at"];
        _EB_eventId = [dictionary valueForKey:@"event_id"];
        _EB_id = [dictionary valueForKey:@"id"];
        _EB_slot = [dictionary valueForKey:@"slot"];
        _EB_startTime = [dictionary valueForKey:@"start_time"];
        _EB_startedAt = [dictionary valueForKey:@"started_at"];
        _EB_active = [dictionary valueForKey:@"Active"];
        _EB_title = [dictionary valueForKey:@"title"];
        _EB_venue = [dictionary valueForKey:@"venue"];
        
        _userEvent = [[UserEvents alloc] init];
        
    }
    return self;
}

@end
