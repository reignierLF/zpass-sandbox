//
//  EventViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 22/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "SwitchViewController.h"

#import "BackButton.h" // Currently disable
#import "SettingsBar.h"

@interface SwitchViewController ()

@property (nonatomic, strong) BackButton *backButton; // Currently disable

@property (nonatomic, strong) UINavigationController *switchViewtNav;
@property (nonatomic, strong) CourseViewController *cViewController;
@property (nonatomic, strong) ScanViewController *sViewController;
@property (nonatomic, strong) AttendanceViewController *aViewController;

@property (nonatomic, strong) UIView *underline;

@end

@implementation SwitchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    /*
     * Note
     * This ViewController is acting container switching 
     * view for Course and Attendance
     */
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    /*
     * SettingsBar for custom navigationBarButton at top right corner
     */
    
    SettingsBar *sb = [[SettingsBar alloc] initWithTarget:self];
    [sb addSettingsBarImage:@"settings-ios"];
    
    /*
     * Titles for segmented control - String only
     * Set default selected index to = 0 / CourseViewController
     */
    
    NSArray *switchArray = [[NSArray alloc] initWithObjects:@"Course",@"Attendance", nil];
    
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:switchArray];
    segmentedControl.frame = CGRectMake(-3, _screenYOffSet, _screenWidth + 6, 50);
    segmentedControl.selectedSegmentIndex = 0;
    
    /*
     * Color for currently selected segment
     */
    
    [segmentedControl setTintColor:[UIColor whiteColor]];
    
    /*
     * Color for not currently selected segment
     */
    
    [segmentedControl setBackgroundColor:[UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f  blue:240.0f/255.0f  alpha:1]];
    
    /*
     * Customize text color,font .etc segmented control for NormalState and SelectedState
     */
    
    // NormalState
    NSDictionary *attNormalState = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"Roboto-Light" size:16],NSFontAttributeName,
                                [UIColor blackColor],NSForegroundColorAttributeName,
                                nil];
    [segmentedControl setTitleTextAttributes:attNormalState forState:UIControlStateNormal];
    
    // Selected State
    NSDictionary *attSelectedState = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
    [segmentedControl setTitleTextAttributes:attSelectedState forState:UIControlStateSelected];
    
    /*
     * END for customizing segmented control
     */
    
    [segmentedControl addTarget:self action:@selector(switchView:) forControlEvents: UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
    /*
     * Add CourseViewController and AttendanceViewController
     * This 2 ViewController to 2nd navigationController for switching view
     */
    
    _cViewController = [[CourseViewController alloc] init];
    _cViewController.delegate = self;
    _cViewController.screenYOffSet = _screenYOffSet;
    _cViewController.screenWidth = _screenWidth;
    _cViewController.screenHeight = _screenHeight - segmentedControl.frame.size.height;
    _cViewController.image = _image;
    _cViewController.courseTitle = _courseTitle;
    _cViewController.courseDate = _courseDate;
    _cViewController.courseTime = _courseTime;
    
    _aViewController = [[AttendanceViewController alloc] init];
    _aViewController.screenYOffSet = _screenYOffSet;
    _aViewController.screenWidth = _screenWidth;
    _aViewController.screenHeight = _screenHeight - segmentedControl.frame.size.height;
    
    /*
     * This event navigationController is for switching view 
     * of CourseViewController and AttendanceViewController
     * Make CourseViewController as rootViewController of event navigation controller
     */
    
    _switchViewtNav = [[UINavigationController alloc] initWithRootViewController:_cViewController];
    _switchViewtNav.view.frame = CGRectMake(0, segmentedControl.frame.origin.y + segmentedControl.frame.size.height, _screenWidth, _screenHeight - segmentedControl.frame.size.height);
    _switchViewtNav.navigationBarHidden = YES;
    [self.view addSubview:_switchViewtNav.view];
    
    /*
     * Underline for segment controller
     * Act as indicator of which view is currently on the screen
     */
    
    _underline = [[UIView alloc] initWithFrame:CGRectMake(0, (segmentedControl.frame.origin.y + segmentedControl.frame.size.height) - 4, (segmentedControl.frame.size.width / 2) - 3, 4)];
    _underline.backgroundColor = [UIColor colorWithRed:23.0f/255.0f green:95.0f/255.0f blue:174.0f/255.0f alpha:1];
    [self.view addSubview:_underline];
    
    /*
     * Custom back button for navigation bar
     *
     
    _backButton = [BackButton buttonWithType:UIButtonTypeCustom];
    _backButton.frame = CGRectMake(-50, 20, 100, 50);
    _backButton.titleLabel.text = @"Back";
    _backButton.titleLabel.font = [UIFont systemFontOfSize:18];
    _backButton.alpha = 0;
    _backButton.targetNavigationController = self.navigationController;
    //backButton.hidden = YES;
    //backButton.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:_backButton];
    
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         //backButton.hidden = NO;
                         _backButton.frame = CGRectMake(0, 20, 100, 50);
                         _backButton.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                         }
                     }];
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)switchView:(UISegmentedControl *)segmentedControl{
    /*
     * Event for switching view
     * Underline transition animation
     */
    
    /*
     * Check which segment is pressed/clicked
     */
    
    if (segmentedControl.selectedSegmentIndex == 0) {
        [UIView animateWithDuration: 0.2
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations: ^{
                             
                             /*
                              * Animation blocks
                              * Set behaviour or style of the animation
                              */
                             
                             _underline.alpha = 0;
                             
                         }completion:^(BOOL finished){
                             if(finished){
                                 
                                 /*
                                  * After the 1st animation is done
                                  * 2nd animation is executed
                                  */
                                 
                                 _underline.frame = CGRectMake(0, _underline.frame.origin.y, _underline.frame.size.width, _underline.frame.size.height);
                                 
                                 [UIView animateWithDuration: 0.2
                                                       delay: 0.0
                                                     options: UIViewAnimationOptionCurveEaseInOut
                                                  animations: ^{
                                                      
                                                      /*
                                                       * Animation blocks
                                                       * Set behaviour or style of the animation
                                                       */
                                                      
                                                      _underline.alpha = 1;
                                                      
                                                  }completion:^(BOOL finished){
                                                      if(finished){
                                                          
                                                      }
                                                  }];
                             }
                         }];
        
        /*
         * Switch current view to Course screen without animation
         */
        
        [_switchViewtNav popToRootViewControllerAnimated:NO];
        
    } else if(segmentedControl.selectedSegmentIndex == 1) {
        [UIView animateWithDuration: 0.2
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations: ^{
                             
                             /*
                              * Animation blocks
                              * Set behaviour or style of the animation
                              */
                             
                             _underline.alpha = 0;
                             
                         }completion:^(BOOL finished){
                             if(finished){
                                 
                                 /*
                                  * After the 1st animation is done
                                  * 2nd animation is executed
                                  */
                                 
                                 _underline.frame = CGRectMake(_underline.frame.size.width, _underline.frame.origin.y, _underline.frame.size.width, _underline.frame.size.height);
                                 
                                 [UIView animateWithDuration: 0.2
                                                       delay: 0.0
                                                     options: UIViewAnimationOptionCurveEaseInOut
                                                  animations: ^{
                                                      
                                                      /*
                                                       * Animation blocks
                                                       * Set behaviour or style of the animation
                                                       */
                                                      
                                                      _underline.alpha = 1;
                                                      
                                                  }
                                                  completion:^(BOOL finished){
                                                      if(finished){
                                                          
                                                      }
                                                  }];
                             }
                         }];
        
        /*
         * Switch current view to Attendance screen without animation
         */
        
        [_switchViewtNav pushViewController:_aViewController animated:NO];
    }
    
    /*
     * Custom event and animation for back button for navigation bar
     *
    
    _backButton.frame = CGRectMake(-50, 20, 100, 50);
    _backButton.alpha = 0;
    
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         //backButton.hidden = NO;
                         _backButton.frame = CGRectMake(0, 20, 100, 50);
                         _backButton.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             
                         }
                     }];
     */
}

/*
 * ==================== Delegates ====================
 */

/*
 * This delegates is from ScanAttendanceViewController Class
 */

-(void)courseStartScan{
    
    /*
     * This will proceed to ScanViewController after pressing/clicking "SCAN QR CODE"
     */
    
    _sViewController = [[ScanViewController alloc] init];
    _sViewController.screenYOffSet = _screenYOffSet;
    [self.navigationController pushViewController:_sViewController animated:YES];
}
@end
