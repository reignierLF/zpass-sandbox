//
//  AttendanceViewController.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 23/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AttendanceCellView.h"

@interface AttendanceViewController : UIViewController

/*
 * This global vars
 * Used for getting/access data from other controller
 */

@property (nonatomic, assign) float screenYOffSet;
@property (nonatomic, assign) float screenWidth;
@property (nonatomic, assign) float screenHeight;

@end
