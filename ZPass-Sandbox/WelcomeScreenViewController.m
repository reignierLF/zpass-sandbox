//
//  WelcomeScreenViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 27/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "WelcomeScreenViewController.h"
#import "ViewController.h"

@interface WelcomeScreenViewController ()

@property (nonatomic) int pages;
@property (nonatomic, strong) UIPageControl *pgCtr;
@property (nonatomic, strong) UILabel *screenLabel;

@end

@implementation WelcomeScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _pages = 5;
    
    /*
    UIPageViewController *pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    pageController.dataSource = self;
    [[pageController view] setFrame:[[self view] bounds]];
    
    APPChildViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:pageController];
    [[self view] addSubview:[pageController view]];
    [pageController didMoveToParentViewController:self];
    */
    //self.view.backgroundColor = [UIColor orangeColor];
    
    float screenWidth = self.view.frame.size.width;
    float screenHeight = self.view.frame.size.height;
    
    UIScrollView *pagerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    pagerScrollView.delegate = self;
    pagerScrollView.bounces = NO;
    pagerScrollView.backgroundColor = [UIColor blackColor];
    pagerScrollView.pagingEnabled = YES;
    pagerScrollView.contentSize = CGSizeMake(pagerScrollView.frame.size.width * _pages, pagerScrollView.frame.size.height- 20);
    [self.view addSubview:pagerScrollView];
    
    _pgCtr = [[UIPageControl alloc] initWithFrame:CGRectMake(0, screenHeight - 30, screenWidth, 30)];
    //_pgCtr.backgroundColor = [UIColor blackColor];
    _pgCtr.numberOfPages = _pages;
    _pgCtr.autoresizingMask=UIViewAutoresizingNone;
    [self.view addSubview:_pgCtr];
    
    NSLog(@"main screen %f", pagerScrollView.frame.size.height);
    NSLog(@"content size %f", pagerScrollView.contentSize.height);
    
    /*
    UIView *page1View = [[UIView alloc] initWithFrame:CGRectMake(0, -20, self.view.frame.size.width, pagerScrollView.contentSize.height + 20)];
    page1View.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Login BG"]];
    [pagerScrollView addSubview:page1View];
    */
    
    
    
    for (int i = 0; i < _pgCtr.numberOfPages; i++) {
        UIImageView *eventImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"natureBG%i",i + 1]]];
        //UIImageView *eventImageView = [[UIImageView alloc] init];
        eventImageView.frame = CGRectMake(screenWidth * i, -20, screenWidth, pagerScrollView.contentSize.height + 20);
        eventImageView.contentMode = UIViewContentModeScaleAspectFill;
        eventImageView.clipsToBounds = YES;
        //eventImageView.translatesAutoresizingMaskIntoConstraints = NO;
        eventImageView.backgroundColor = [UIColor orangeColor];
        
        [pagerScrollView addSubview:eventImageView];
    }
    
    _screenLabel = [[UILabel alloc] initWithFrame:CGRectMake((screenWidth / 2) - 100, (screenHeight / 2) - 20, 200, 40)];
    _screenLabel.text = [NSString stringWithFormat:@"Screen number %li",(long)_pgCtr.currentPage];
    _screenLabel.textAlignment = NSTextAlignmentCenter;
    _screenLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:_screenLabel];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake((pagerScrollView.contentSize.width) - ((pagerScrollView.frame.size.width / 2) + 70), _screenLabel.frame.origin.y + _screenLabel.frame.size.height + 80, 140, 30);
    [closeButton setTitle:@"Close" forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    //loginButton.backgroundColor = [UIColor greenColor];
    closeButton.layer.cornerRadius = 5;
    closeButton.layer.borderWidth = 1;
    closeButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    [pagerScrollView addSubview:closeButton];
    
    NSLog(@"%f",closeButton.frame.origin.x);
    
    NSLog(@"%f",closeButton.frame.origin.y);
    
    [closeButton addTarget:self action:@selector(closeEvent) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)closeEvent{
    ViewController *vc = [[ViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    _pgCtr.currentPage = fabs(((scrollView.contentSize.width - scrollView.contentOffset.x) / scrollView.frame.size.width) - _pages);
    _screenLabel.text = [NSString stringWithFormat:@"Screen number %li",(long)_pgCtr.currentPage];
}
@end
