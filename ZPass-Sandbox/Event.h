//
//  Event.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 30/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventBatches.h"

@interface Event : NSObject

@property (nonatomic, assign) NSString *E_companyId;
@property (nonatomic, assign) NSString *E_createdAt;
@property (nonatomic, assign) NSString *E_createdById;
@property (nonatomic, assign) NSString *E_credit;
@property (nonatomic, assign) NSString *E_description;
@property (nonatomic, assign) NSString *E_draftId;
@property (nonatomic, assign) NSString *E_id;
@property (nonatomic, assign) NSString *E_imageUrlLarge;
@property (nonatomic, assign) NSString *E_imageUrlMedium;
@property (nonatomic, assign) NSString *E_imageUrlSmall;
@property (nonatomic, assign) NSString *E_organizerInformation;
@property (nonatomic, assign) NSString *E_previewDescription;
@property (nonatomic, assign) NSString *E_publishedAt;
@property (nonatomic, assign) NSString *E_status;
@property (nonatomic, assign) NSString *E_submittedAt;
@property (nonatomic, assign) NSString *E_title;

@property (nonatomic, assign) NSArray *E_categoryList;

@property (nonatomic, strong) EventBatches *eventBatches;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
