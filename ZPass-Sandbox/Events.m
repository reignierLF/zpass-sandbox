//
//  Events.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 30/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "Events.h"

@implementation Events

-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
        _event = [[Event alloc] initWithDictionary:dictionary];
    }
    return self;
}

@end
