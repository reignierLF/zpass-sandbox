//
//  GradientAnimationColor.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 24/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "GradientAnimationColor.h"

@implementation GradientAnimationColor{
    NSArray *_colorsFirst;
    NSArray *_colorsSecond;
    NSArray *_currentColor;
    NSTimer *colorTimer;
    float time;
}

+ (Class)layerClass {
    return [CAGradientLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CAGradientLayer *layer = (CAGradientLayer *)[self layer];
        _colorsFirst = @[(__bridge id)[[UIColor lightTextColor] CGColor], (__bridge id)[[UIColor blackColor] CGColor]];
        _colorsSecond = @[(__bridge id)[[UIColor blackColor] CGColor], (__bridge id)[[UIColor lightTextColor] CGColor]];
        _currentColor = _colorsFirst;
        [layer setColors:_colorsFirst];

        time = 0;
        //[self animateColors];
        
        colorTimer = [NSTimer scheduledTimerWithTimeInterval:4.0
                                                      target:self selector:@selector(animateColors) userInfo:nil repeats:YES];
    }
    return self;
}

- (void)animateColors {
    
    if (_currentColor == _colorsSecond) {
        _currentColor = _colorsFirst;
    } else {
        _currentColor = _colorsSecond;
    }
    
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"colors"];
    anim.fromValue = [[self layer] valueForKey:@"colors"];
    anim.toValue = _currentColor;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.duration = 4.0;
    anim.delegate = self;
    [self.layer addAnimation:anim forKey:@"colors"];
    
    //NSLog(@"animating");
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag {
    CAGradientLayer *layer = (CAGradientLayer *)[self layer];
    [layer setColors:_currentColor];
    
    
    //[colorTimer invalidate];
    //colorTimer = nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
