//
//  EventBatches.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 30/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserEvents.h"

@interface EventBatches : NSObject

@property (nonatomic, assign) NSString *EB_credit;
@property (nonatomic, assign) NSString *EB_duration;
@property (nonatomic, assign) NSString *EB_endTime;
@property (nonatomic, assign) NSString *EB_endedAt;
@property (nonatomic, assign) NSString *EB_eventId;
@property (nonatomic, assign) NSString *EB_id;
@property (nonatomic, assign) NSString *EB_slot;
@property (nonatomic, assign) NSString *EB_startTime;
@property (nonatomic, assign) NSString *EB_startedAt;
@property (nonatomic, assign) NSString *EB_active;
@property (nonatomic, assign) NSString *EB_title;
@property (nonatomic, assign) NSString *EB_venue;

@property (nonatomic, strong) UserEvents *userEvent;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
