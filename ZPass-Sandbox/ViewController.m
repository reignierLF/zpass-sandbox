//
//  ViewController.m
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 20/6/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import "ViewController.h"
#import "HomeViewController.h"

#import "JsonParser.h"
#import "AssetsModification.h"

@interface ViewController ()

@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UITextField *passwordTextField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    JsonParser *jsp = [[JsonParser alloc] init];
    
    [jsp parseJsonURL:@"http://www.goooooogle.com/" parsing:^(BOOL isComplete){
        if(!isComplete){
            NSLog(@"not complete");
            //Json parsing not complete
        }else{
            NSLog(@"complete");
            //Json parsing complete
            
            NSLog(@"JSON : %@",jsp.jsonDictionary);
        }
    }];
    
    /*
     * This remove the "welcome screen page" from navigation controller and make
     * the "login screen" as root of navigation controller
     *
     
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    [viewControllers removeObjectAtIndex:0];
    [self.navigationController setViewControllers:viewControllers];
    */
    
    /*
     * This let resize the image/picture to desire frame
     * Using AssetsModification may reduce the quality of image
     * AssetModification did use only here for background image
     */
    
    AssetsModification *as = [[AssetsModification alloc] init];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[as imageResizeByBound:@"zpass login bg" targetView:self.view]];
    
    UIImage *logo = [UIImage imageNamed:@"ZPass Logo"];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logo];
    //logoImageView.backgroundColor = [UIColor greenColor];
    logoImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:logoImageView];
    
    /*
     * NOTE
     * Auto-layout is done programmatically
     */

    /*
     * Put the logoImage in the center of X-axis
     */
    
    NSLayoutConstraint *logoImageViewXConstraint = [NSLayoutConstraint
                                                    constraintWithItem: logoImageView
                                                             attribute: NSLayoutAttributeCenterX
                                                             relatedBy: NSLayoutRelationGreaterThanOrEqual
                                                                toItem: self.view
                                                             attribute: NSLayoutAttributeCenterX
                                                            multiplier: 1.0
                                                              constant: 0];
    /*
     * Put the logoImage in the center of Y-axis
     */
    
    NSLayoutConstraint *logoImageViewYConstraint = [NSLayoutConstraint
                                                    constraintWithItem: logoImageView
                                                             attribute: NSLayoutAttributeCenterY
                                                             relatedBy: NSLayoutRelationEqual
                                                                toItem: self.view
                                                             attribute: NSLayoutAttributeCenterY
                                                            multiplier: 1.0f
                                                              constant: -logoImageView.frame.size.height / 2];
    /*
     * Resize the width of logoImage
     */
    
    NSLayoutConstraint *logoImageViewsWidth = [NSLayoutConstraint
                                               constraintWithItem: logoImageView
                                                        attribute: NSLayoutAttributeWidth
                                                        relatedBy: NSLayoutRelationEqual
                                                           toItem: nil
                                                        attribute: NSLayoutAttributeNotAnAttribute
                                                       multiplier: 1.0
                                                         constant: self.view.frame.size.width / 2];
    
    /*
     * Resize the height of logoImage
     */
    
    NSLayoutConstraint *logoImageViewHeight = [NSLayoutConstraint
                                               constraintWithItem: logoImageView
                                                        attribute: NSLayoutAttributeHeight
                                                        relatedBy: NSLayoutRelationEqual
                                                           toItem: nil
                                                        attribute: NSLayoutAttributeNotAnAttribute
                                                       multiplier: 1.0
                                                         constant: self.view.frame.size.width / 2];
    
    /*
     * Compile/Add all Constraints for logoImage
     */
    
    [self.view addConstraints:@[ logoImageViewXConstraint,
                                 logoImageViewYConstraint,
                                 logoImageViewsWidth,
                                 logoImageViewHeight]];
    
    _emailTextField = [[UITextField alloc] init];
    //emailTextField.backgroundColor = [UIColor greenColor];
    _emailTextField.translatesAutoresizingMaskIntoConstraints = NO;
    _emailTextField.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    _emailTextField.delegate = self;
    _emailTextField.text = @"Email address";
    _emailTextField.font = [UIFont fontWithName:@"Roboto-Light" size:16];
    [self.view addSubview:_emailTextField];
    
    /*
     * Aligning the emailTextField base on top of emailTextField
     * and the bottom of logoImageView with margin value of 10px
     */
    
    NSLayoutConstraint *emailTextFieldTopConstraint = [NSLayoutConstraint
                                                       constraintWithItem: _emailTextField
                                                                attribute: NSLayoutAttributeTop
                                                                relatedBy: NSLayoutRelationGreaterThanOrEqual
                                                                   toItem: logoImageView
                                                                attribute: NSLayoutAttributeBottom
                                                               multiplier: 1.0
                                                                 constant: 10];
    
    /*
     * Assign the height of emailTextField with a value of 30px
     * emailTextField's height = 30
     */
    
    NSLayoutConstraint *emailTextFieldHeightConstraint = [NSLayoutConstraint
                                                          constraintWithItem: _emailTextField
                                                                   attribute: NSLayoutAttributeHeight
                                                                   relatedBy: NSLayoutRelationEqual
                                                                      toItem: nil
                                                                   attribute: NSLayoutAttributeNotAnAttribute
                                                                  multiplier: 1.0
                                                                    constant: 30];
    
    /*
     * Stretch the emailTextField to the left side of the screen
     * with a margin value of 50px
     */
    
    NSLayoutConstraint *emailTextFieldLeftConstraint = [NSLayoutConstraint
                                                        constraintWithItem: _emailTextField
                                                                 attribute: NSLayoutAttributeLeft
                                                                 relatedBy: NSLayoutRelationEqual
                                                                    toItem: self.view
                                                                 attribute: NSLayoutAttributeLeft
                                                                multiplier: 1.0
                                                                  constant: 50];
    
    /*
     * Stretch the emailTextField to the right side of the screen
     * with a margin value of 50px
     */
    
    NSLayoutConstraint *emailTextFieldRightConstraint = [NSLayoutConstraint
                                                         constraintWithItem: _emailTextField
                                                                  attribute: NSLayoutAttributeRight
                                                                  relatedBy: NSLayoutRelationEqual
                                                                     toItem: self.view
                                                                  attribute: NSLayoutAttributeRight
                                                                 multiplier: 1.0
                                                                   constant: -50];
    
    /*
     * Compile/Add all Constraints for emailTextField
     */
    
    [self.view addConstraints:@[ emailTextFieldTopConstraint,
                                 emailTextFieldHeightConstraint,
                                 emailTextFieldLeftConstraint,
                                 emailTextFieldRightConstraint]];
    
    /*
     * Instantiate underline for emailTextField
     */
    
    UIView *emailLineView = [[UIView alloc] init];
    emailLineView.translatesAutoresizingMaskIntoConstraints = NO;
    emailLineView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:emailLineView];
    
    /*
     * Aligning the emailLineView base on top of emailLineView
     * and the bottom of emailTextField with margin value of 0px
     */
    
    NSLayoutConstraint *lineViewTopConstraint = [NSLayoutConstraint
                                                 constraintWithItem: emailLineView
                                                          attribute: NSLayoutAttributeTop
                                                          relatedBy: NSLayoutRelationGreaterThanOrEqual
                                                             toItem: _emailTextField
                                                          attribute: NSLayoutAttributeBottom
                                                         multiplier: 1
                                                           constant: 0];
    
    /*
     * Assign the height of emailLineView with a value of 1px
     * emailLineView's height = 1
     */
    
    NSLayoutConstraint *lineViewHeightConstraint = [NSLayoutConstraint
                                                    constraintWithItem: emailLineView
                                                             attribute: NSLayoutAttributeHeight
                                                             relatedBy: NSLayoutRelationEqual
                                                                toItem: nil
                                                             attribute: NSLayoutAttributeNotAnAttribute
                                                            multiplier: 1.0
                                                              constant: 1];
    
    /*
     * Stretch the emailLineView to the left side of the screen
     * with a margin value of 40px
     */
    
    NSLayoutConstraint *lineViewConstraintLeft = [NSLayoutConstraint
                                                  constraintWithItem: emailLineView
                                                           attribute: NSLayoutAttributeLeft
                                                           relatedBy: NSLayoutRelationEqual
                                                              toItem: self.view
                                                           attribute: NSLayoutAttributeLeft
                                                          multiplier: 1.0
                                                            constant: 40];
    
    /*
     * Stretch the emailLineView to the right side of the screen
     * with a margin value of 40px
     */
    
    NSLayoutConstraint *lineViewConstraintRight = [NSLayoutConstraint
                                                   constraintWithItem: emailLineView
                                                            attribute: NSLayoutAttributeRight
                                                            relatedBy: NSLayoutRelationEqual
                                                               toItem: self.view
                                                            attribute: NSLayoutAttributeRight
                                                           multiplier: 1.0
                                                             constant: -40];
    
    /*
     * Compile/Add all Constraints for emailLineView
     */
    
    [self.view addConstraints:@[ lineViewTopConstraint,
                                 lineViewHeightConstraint,
                                 lineViewConstraintLeft,
                                 lineViewConstraintRight]];
    
    _passwordTextField = [[UITextField alloc] init];
    //passwordTextField.backgroundColor = [UIColor greenColor];
    _passwordTextField.translatesAutoresizingMaskIntoConstraints = NO;
    _passwordTextField.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    _passwordTextField.delegate = self;
    _passwordTextField.text = @"Password";
    _passwordTextField.font = [UIFont fontWithName:@"Roboto-Light" size:16];
    [self.view addSubview:_passwordTextField];
    
    /*
     * Aligning the passwordTextField base on top of passwordTextField
     * and the bottom of emailTextField with margin value of 20px
     */
    
    NSLayoutConstraint *passwordTextFieldTopConstraint = [NSLayoutConstraint
                                                          constraintWithItem: _passwordTextField
                                                                   attribute: NSLayoutAttributeTop
                                                                   relatedBy: NSLayoutRelationGreaterThanOrEqual
                                                                      toItem: _emailTextField
                                                                   attribute: NSLayoutAttributeBottom
                                                                  multiplier: 1.0
                                                                    constant: 20];
    
    /*
     * Assign the height of passwordTextField with a value of 30px
     * passwordTextField height = 30
     */
    
    NSLayoutConstraint *passwordTextFieldHeightConstraint = [NSLayoutConstraint
                                                             constraintWithItem: _passwordTextField
                                                                      attribute: NSLayoutAttributeHeight
                                                                      relatedBy: NSLayoutRelationEqual
                                                                         toItem: nil
                                                                      attribute: NSLayoutAttributeNotAnAttribute
                                                                     multiplier: 1.0
                                                                       constant: 30];
    
    /*
     * Stretch the passwordTextField to the left side of the screen
     * with a margin value of 50px
     */
    
    NSLayoutConstraint *passwordTextFieldLeftConstraint = [NSLayoutConstraint
                                                           constraintWithItem: _passwordTextField
                                                                    attribute: NSLayoutAttributeLeft
                                                                    relatedBy: NSLayoutRelationEqual
                                                                       toItem: self.view
                                                                    attribute: NSLayoutAttributeLeft
                                                                   multiplier: 1.0
                                                                     constant: 50];
    
    /*
     * Stretch the passwordTextField to the right side of the screen
     * with a margin value of 50px
     */
    
    NSLayoutConstraint *passwordTextFieldRightConstraint = [NSLayoutConstraint
                                                            constraintWithItem: _passwordTextField
                                                                     attribute: NSLayoutAttributeRight
                                                                     relatedBy: NSLayoutRelationEqual
                                                                        toItem: self.view
                                                                     attribute: NSLayoutAttributeRight
                                                                    multiplier: 1.0
                                                                      constant: -50];
    
    /*
     * Compile/Add all Constraints for passwordTextField
     */
    
    [self.view addConstraints:@[ passwordTextFieldTopConstraint,
                                 passwordTextFieldHeightConstraint,
                                 passwordTextFieldLeftConstraint,
                                 passwordTextFieldRightConstraint]];
    
    /*
     * Instantiate underline for passwordTextField
     */
    
    UIView *passwordLineView = [[UIView alloc] init];
    passwordLineView.backgroundColor = [UIColor lightGrayColor];
    passwordLineView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:passwordLineView];
    
    /*
     * Aligning the passwordLineView base on top of passwordLineView
     * and the bottom of passwordTextField with margin value of 0px
     */
    
    NSLayoutConstraint *passwordLineViewTopConstraint = [NSLayoutConstraint
                                                         constraintWithItem: passwordLineView
                                                                  attribute: NSLayoutAttributeTop
                                                                  relatedBy: NSLayoutRelationGreaterThanOrEqual
                                                                     toItem: _passwordTextField
                                                                  attribute: NSLayoutAttributeBottom
                                                                 multiplier: 1
                                                                   constant: 0];
    
    /*
     * Assign the height of passwordLineView with a value of 1px
     * passwordLineView's height = 1
     */
    
    NSLayoutConstraint *passwordLineViewHeightConstraint = [NSLayoutConstraint
                                                            constraintWithItem: passwordLineView
                                                                     attribute: NSLayoutAttributeHeight
                                                                     relatedBy: NSLayoutRelationEqual
                                                                        toItem: nil
                                                                     attribute: NSLayoutAttributeNotAnAttribute
                                                                    multiplier: 1.0
                                                                      constant: 1];
    
    /*
     * Stretch the passwordLineView to the left side of the screen
     * with a margin value of 40px
     */
    
    NSLayoutConstraint *passwordLineViewLeftConstraint = [NSLayoutConstraint
                                                          constraintWithItem: passwordLineView
                                                                   attribute: NSLayoutAttributeLeft
                                                                   relatedBy: NSLayoutRelationEqual
                                                                      toItem: self.view
                                                                   attribute: NSLayoutAttributeLeft
                                                                  multiplier: 1.0
                                                                    constant: 40];
    
    /*
     * Stretch the passwordLineView to the right side of the screen
     * with a margin value of 40px
     */
    
    NSLayoutConstraint *passwordLineViewRightConstraint = [NSLayoutConstraint
                                                           constraintWithItem: passwordLineView
                                                                    attribute: NSLayoutAttributeRight
                                                                    relatedBy: NSLayoutRelationEqual
                                                                       toItem: self.view
                                                                    attribute: NSLayoutAttributeRight
                                                                   multiplier: 1.0
                                                                     constant: -40];
    
    /*
     * Compile/Add all Constraints for passwordLineView
     */
    
    [self.view addConstraints:@[ passwordLineViewTopConstraint,
                                 passwordLineViewHeightConstraint,
                                 passwordLineViewLeftConstraint,
                                 passwordLineViewRightConstraint]];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    [loginButton setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.7] forState:UIControlStateNormal];
    [loginButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:16]];
    //loginButton.backgroundColor = [UIColor greenColor];
    loginButton.translatesAutoresizingMaskIntoConstraints = NO;
    loginButton.layer.cornerRadius = 5;
    loginButton.layer.borderWidth = 1;
    loginButton.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self.view addSubview:loginButton];
    
    [loginButton addTarget:self action:@selector(loginEvent) forControlEvents:UIControlEventTouchUpInside];
    
    /*
     * Aligning the loginButton base on top of loginButton
     * and the bottom of passwordTextField with margin value of 0px
     */

    NSLayoutConstraint *loginButtonTopConstraint = [NSLayoutConstraint
                                                    constraintWithItem: loginButton
                                                             attribute: NSLayoutAttributeTop
                                                             relatedBy: NSLayoutRelationGreaterThanOrEqual
                                                                toItem: _passwordTextField
                                                             attribute: NSLayoutAttributeBottom
                                                            multiplier: 1.0
                                                              constant: 30];
    
    /*
     * Assign the height of loginButton with a value of 40px
     * passwordLineView's height = 40
     */
    
    NSLayoutConstraint *loginButtonHeightConstraint = [NSLayoutConstraint
                                                       constraintWithItem: loginButton
                                                                attribute: NSLayoutAttributeHeight
                                                                relatedBy: NSLayoutRelationEqual
                                                                   toItem: nil
                                                                attribute: NSLayoutAttributeNotAnAttribute
                                                               multiplier: 1.0
                                                                 constant: 40];
    
    /*
     * Aligning the loginButton base on bottom of loginButton
     * and the bottom of screen with margin value of 80px
     */
    
    NSLayoutConstraint *loginButtonBottomConstraint = [NSLayoutConstraint
                                                       constraintWithItem: loginButton
                                                                attribute: NSLayoutAttributeBottom
                                                                relatedBy: NSLayoutRelationGreaterThanOrEqual
                                                                   toItem: self.view
                                                                attribute: NSLayoutAttributeBottom
                                                               multiplier: 1.0f
                                                                 constant: -80];
    
    /*
     * Stretch the loginButton to the left side of the screen
     * with a margin value of 40px
     */
    
    NSLayoutConstraint *loginButtonLeftConstraint = [NSLayoutConstraint
                                                     constraintWithItem: loginButton
                                                              attribute: NSLayoutAttributeLeft
                                                              relatedBy: NSLayoutRelationEqual
                                                                 toItem: self.view
                                                              attribute: NSLayoutAttributeLeft
                                                             multiplier: 1.0
                                                               constant: 40];
    
    /*
     * Stretch the loginButton to the right side of the screen
     * with a margin value of 40px
     */
    
    NSLayoutConstraint *loginButtonRightConstraint = [NSLayoutConstraint
                                                      constraintWithItem: loginButton
                                                               attribute: NSLayoutAttributeRight
                                                               relatedBy: NSLayoutRelationEqual
                                                                  toItem: self.view
                                                               attribute: NSLayoutAttributeRight
                                                              multiplier: 1.0
                                                                constant: -40];
    
    /*
     * Compile/Add all Constraints for loginButton
     */
    
    [self.view addConstraints:@[ loginButtonTopConstraint,
                                 loginButtonHeightConstraint,
                                 loginButtonBottomConstraint,
                                 loginButtonLeftConstraint,
                                 loginButtonRightConstraint]];
    
    
    /*
     * Detect keyboard whether will show or hide
     * Used for screen to move up base on the height of keyboard
     * and so it doest overlap the keyboard over the email, password input and login button
     */
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillShow:)
                                                 name: UIKeyboardWillChangeFrameNotification
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillBeHidden:)
                                                 name: UIKeyboardWillHideNotification
                                               object: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    /*
     * Dont show navigation bar at login screen
     */
    
    self.navigationController.navigationBarHidden = YES;
}

-(void)loginEvent{
    /*
     * Hide the keyboard after loginButton has been press
     */
    
    [_emailTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
    
    /*
     * Instantiate HomeViewController and proceed to its page/screen
     * with animation transition
     */
    
    HomeViewController *homeVC = [[HomeViewController alloc] init];
    [self.navigationController pushViewController:homeVC animated:YES];
    
    NSLog(@"Logging-in...");
    NSLog(@"Proceed to %@",homeVC.description);
}

-(void)keyboardWillShow:(NSNotification *)notification {
    
    /*
     * Get the height of the keyboard and subtract 30px
     * the 30px is the auto-complete words appear at the 
     * top of keyboard
     */
    
    float keyBoardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height - 30;
    
    /*
     * Check if screen is on default position
     * Screen y-axis = 0
     */
    
    if(self.view.frame.origin.y >= 0){
        
        /*
         * Move the screen upward base on the height of the keyboard
         * Screen Y-axis subtract by height of keyboard
         * Animation is done automatically
         */
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - (keyBoardHeight), self.view.frame.size.width, self.view.frame.size.height);
    }
}

-(void)keyboardWillBeHidden:(NSNotification *)notification{
    
    /*
     * Reposition of screen by moving downward
     * Back to default Y-axis position
     */
    
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

-(void)emptyTextField:(UITextField *)textField{
    
    /*
     * Check if text field is empty or no inputs
     */
    
    if(textField.text.length == 0){
        
        /*
         * Check which text field
         *
         * If emailTextField is empty, "Email address" word
         * will be added inside the emailTextField, same goes
         * to passwordTextField
         */
        
        if(textField == _emailTextField){
            _emailTextField.text = @"Email address";
        }else if(textField == _passwordTextField){
            _passwordTextField.text = @"Password";
        }
    }
}

/*
 * ==================== Delegates ====================
 */

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    /*
     * Check if passwordTextField is being click
     */
    
    if(textField == _passwordTextField){
        
        /*
         * Securing the password while typing
         * words/characters will be replace by *
         */
        
        _passwordTextField.secureTextEntry = YES;
        NSLog(@"password being typed but hidden");
    }
    
    /*
     * When the emailTextField/passwordTextField was click
     * remove any inputs in the text field
     */
    
    textField.text = @"";
    
    /*
     * Replace the "Return" key in the keyboard
     * with "Done" key
     * "Return" is the defauult key in the keyboard
     */
    
    [textField setReturnKeyType:UIReturnKeyDone];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    /*
     * If done editting to both text field
     * "emptyTextField" method is called to check
     * if their text field is empty
     */
    
    [self emptyTextField:textField];
    
    /*
     * Check if passwordTextField is being click
     */
    
    if(textField == _passwordTextField){
        
        /*
         * Detect if input is same to the word of "Password"
         * if YES/True, dont replace characters with *
         * if NO/False, replace all the characters with *
         */
        
        if([_passwordTextField.text isEqualToString:@"Password"]){
            _passwordTextField.secureTextEntry = NO;
        }else{
            _passwordTextField.secureTextEntry = YES;
        }
    }
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    /*
     * "emptyTextField" method is called after hitting "Done" key
     * to check if text field is empty
     */
    
    [self emptyTextField:textField];
    
    /*
     * Hide the keyboard
     */
    
    [textField resignFirstResponder];

    return YES;
}

@end
