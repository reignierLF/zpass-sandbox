//
//  SettingsBar.h
//  ZPass-Sandbox
//
//  Created by LF-Mac-Air on 5/7/16.
//  Copyright © 2016 LF-Mac-Air. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SettingsBar : NSObject <UIActionSheetDelegate>

/*
 * Global vars for read only
 */

/*
 * targetViewController var - base on which controller currently displayed 
 *                            can be used on desire view controller
 * 
 * selfDelegate var         - act as like delegate but
 *                            pointing/referring to target view controller
 */

@property (nonatomic, readonly) UIViewController *targetViewController;
@property (nonatomic, readonly) id selfDelegate;

/*
 * Target which view controller but required
 * delegate UIActionSheetDelegate on that target
 */

-(id)initWithTarget:(UIViewController*)vc;

/*
 * This method is required after initWithTarget
 */

-(void)addSettingsBarImage:(NSString*)imageName;

@end
